<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $birthday
 * @property integer $role
 * @property string $address
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $active
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }
    public $old_password;
    public $new_password;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birthday'], 'safe'],
            [['role', 'region_id', 'city_id', 'active'], 'integer'],
            [['first_name', 'last_name', 'email', 'password', 'phone', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'phone' => 'Phone',
            'birthday' => 'Birthday',
            'role' => 'Role',
            'address' => 'Address',
            'region_id' => 'Region ID',
            'city_id' => 'City ID',
            'active' => 'Active',
        ];
    }
    public function getUserName($id) {
        return User::find()->select('username')->where(['id'=> $id])->one()['username'];
    }
}
