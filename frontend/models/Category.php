<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $meta_key
 * @property string $meta_description
 * @property integer $parent_id
 * @property string $name
 * @property string $description
 * @property string $url
 * @property integer $sort
 * @property integer $status
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'status'], 'integer'],
            [['description'], 'string'],
            [['meta_key', 'meta_description', 'name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_key' => 'Meta Key',
            'meta_description' => 'Meta Description',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'description' => 'Description',
            'url' => 'Url',
            'sort' => 'Sort',
            'status' => 'Status',
        ];
    }
    
    
     public function getStructure()

 {

  //запрос к базе данных в $result попадают все записи из таблицы в виде массива

  $result = Category::find()->orderBy(['sort' => SORT_ASC])->asArray()->all();

  //

  if (!$result) {

   return NULL;

  }

  // $arr_cat будет создаваться массив категорий, где индексы, это parent_id

  $arr_cat = array();



  //В цикле формируем массив

  for ($i = 0; $i < count($result);$i++) {

   $row = $result[$i];

   if ($row['parent_id'] == NULL)

   $row['parent_id'] = 0;

   //Формируем массив, где ключами являются id родительской категории

   if (empty($arr_cat[$row['parent_id']])) {

    $arr_cat[$row['parent_id']] = array();

   }

   $arr_cat[$row['parent_id']][] = $row;

  }



// $view_cat - лямда функция для создания массива категорий, который будет передан в отображение

  $view_cat =

  function ($data, $parent_id = 0) use ( & $view_cat)

  {

   $result = NULL;

   if (empty($data[$parent_id])) {

    return;

   }

   $result = array();

    

   //перебираем в цикле массив и выводим на экран

   for ($i = 0; $i < count($data[$parent_id]);$i++) {
    $active = 0;
    if ('/parts/'.$data[$parent_id][$i]['url'] == $_SERVER['REQUEST_URI']) {
        $active = $i+1;

    }

    $result[] = [
    
     'label' => $data[$parent_id][$i]['name'],

     'url' => '/parts/'.$data[$parent_id][$i]['url'],

      //можно пометить какой либо пункт как активный     

     'active' => $data[$parent_id][$i]['id'] == $active,
     
     'options' => ['class' => 'list-group-item' ],

     'items' => $view_cat($data,$data[$parent_id][$i]['id'])];

    //рекурсия - проверяем нет ли дочерних категорий

   }
    
   return $result;

  };

  $result = $view_cat($arr_cat);

  return $result;

 }
    
}
