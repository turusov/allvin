<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_reviews".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_product
 * @property integer $active
 */
class UserReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_product', 'active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_product' => 'Id Product',
            'active' => 'Active',
        ];
    }
    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'id_product'])->with('mainImages');
    }
}
