<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product_modification".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_modification
 */
class ProductModification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_modification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_modification'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'id_modification' => 'Id Modification',
        ];
    }
}
