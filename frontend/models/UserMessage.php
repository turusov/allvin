<?php

namespace frontend\models;

use Yii;



/**
 * This is the model class for table "user_message".
 *
 * @property integer $id
 * @property integer $id_dialog
 * @property integer $id_user
 * @property string $message
 * @property string $date_create
 * @property integer $view
 */
class UserMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dialog', 'id_user', 'reading'], 'integer'],
            [['message'], 'string'],
            [['date_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_dialog' => 'Id Dialog',
            'id_user' => 'Id User',
            'message' => 'Сообщение',
            'date_create' => 'Date Create',
            'view' => 'View',
        ];
    }

    public function create_dialog($id_user, $id_product, $message) {
        $parthModel = new Products;
        $secUser = $parthModel->find()->where(['id'=>$id_product])->select('id_user')->one()['id_user'];

        $dialogModel = new Dialog();
        $dialogModel->id_product = $id_product;
        $dialogModel->active = 1;
        $dialogModel->save();

        $messageModel = new UserMessage;
        $messageModel->id_dialog = $dialogModel->id;
        $messageModel->id_user = $id_user;
        $messageModel->message = $message;
        $messageModel->date_create = date("Y-m-d H:i:s");
        $messageModel->reading = 1;
        $messageModel->save();


        $userDialogModel = new UserDialog();
        $userDialogModel->id_dialog =  $dialogModel->id;
        $userDialogModel->id_user = $id_user;
        $userDialogModel->save();
        $userDialogModel2 = new UserDialog();
        $userDialogModel2->id_dialog =  $dialogModel->id;
        $userDialogModel2->id_user =  $secUser;
        $userDialogModel2->save();
        $notification = new UserNotification();

        $notification->id_user =  $secUser;
        $notification->message = "У вас новое сообщение";

        $notification->save();
        return false;
    }

    public function addMessage($id_dialog, $id_user, $message) {
        $dialogModel = Dialog::find('id', $id_dialog)->one();
        if (!$dialogModel->active) {
            $dialogModel->active = 1;
            $dialogModel->save();
        }
        $messageModel = new UserMessage;
        $messageModel->id_dialog = $id_dialog;
        $messageModel->id_user = $id_user;
        $messageModel->message = $message;
        $messageModel->date_create = date("Y-m-d H:i:s");
        $messageModel->reading = 1;
        $messageModel->save();

        $users = UserDialog::find([
            'id_dialog' => $id_dialog,
        ])->select('id_user')->all();
        $id_secUser = 0;
        foreach ($users as $user) {
            if ($id_user != $user['id_user']) {
                $id_secUser =  $user['id_user'];
            }
        }


        $notification = new UserNotification();

        $notification->id_user = $id_secUser;
        $notification->message = "У вас новое сообщение";

        $notification->save();
        return false;
    }

    public function getLast($id_dialog)
    {
        $userMessageModel = new UserMessage;
        return $userMessageModel->find()->where([
            'id_dialog' => $id_dialog,
        //    'id' => $userMessageModel->find('id_dialog', $id_dialog)->max('id')
        ])->orderBy(['id'=>SORT_DESC])->one();
    }

}
