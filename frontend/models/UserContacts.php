<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_contacts".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $phone
 * @property string $vk
 * @property string $ok
 * @property string $skype
 * @property string $fb
 */
class UserContacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['phone', 'id_user'], 'required'],
            [['phone', 'vk', 'ok', 'skype', 'fb'], 'unique'],
            [['phone', 'vk', 'ok', 'skype', 'fb'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'phone' => 'Телефон',
            'vk' => 'Vk',
            'ok' => 'Ok',
            'skype' => 'Skype',
            'fb' => 'Fb',
        ];
    }
}
