<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product_mark".
 *
 * @property integer $id
 * @property integer $id_mark
 * @property integer $id_product
 */
class ProductMark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mark', 'id_product'], 'integer'],
        ];
    }


    public function getMarks() {
        return $this->hasOne(CarMark::className(), ['id_mark' => 'id_car_mark']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_mark' => 'Марка автомобиля',
            'id_product' => 'Id Product',
        ];
    }
}
