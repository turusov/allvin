<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_dialog".
 *
 * @property integer $id
 * @property integer $id_dialog
 * @property integer $id_user
 */
class UserDialog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_dialog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dialog', 'id_user'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_dialog' => 'Id Dialog',
            'id_user' => 'Пользователь',
        ];
    }

    public function getDialog() {
        return $this->hasOne(Dialog::className(), ['id' => 'id_dialog']);
    }
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

}
