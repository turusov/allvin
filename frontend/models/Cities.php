<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $region_id
 * @property string $title_ru
 * @property string $area_ru
 * @property string $region_ru
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id'], 'required'],
            [['id', 'country_id', 'region_id'], 'integer'],
            [['title_ru', 'area_ru', 'region_ru'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'title_ru' => 'Title Ru',
            'area_ru' => 'Area Ru',
            'region_ru' => 'Region Ru',
        ];
    }
}
