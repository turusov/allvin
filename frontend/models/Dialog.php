<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "dialog".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $active
 */
class Dialog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dialog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'active' => 'Active',
        ];
    }
}
