<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "parts_image".
 *
 * @property integer $Id
 * @property integer $id_product
 * @property string $image
 * @property integer $main
 */
class PartsImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parts_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'main'], 'integer'],
   
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'id_product' => 'Id Product',
            'image' => 'Image',
            'main' => 'Main',
        ];
    }

    public function getImageById($id) {
        return PartsImage::find()->select('image')->where(['id_product'=>$id, 'main'=>1])->one()['image'];
    }
}
