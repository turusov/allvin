<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "parts_brand".
 *
 * @property integer $id
 * @property string $name
 * @property string $img
 * @property string $description
 */
class PartsBrand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parts_brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img' => 'Img',
            'description' => 'Description',
        ];
    }
}
