<?php

namespace frontend\models;
use Yii;

/**
 * This is the model class for table "categories_product".
 *
 * @property integer $id
 * @property integer $id_categories
 * @property integer $id_product
 */
class CategoriesProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_categories', 'id_product'], 'integer'],
            [['id_categories', 'id_product'],  'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_categories' => 'Категория',
            'id_product' => 'Автозапчасть',
        ];
    }
}
