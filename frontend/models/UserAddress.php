<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_address".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_region
 * @property integer $id_city
 * @property string $address
 * @property integer $main
 */
class UserAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'],  'required'],
            [['id_user', 'id_region', 'id_city', 'main'], 'integer'],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_region' => 'Регион',
            'id_city' => 'Город',
            'address' => 'Адрес',
            'main' => 'Основной',
        ];
    }

    public function getRegions() {
        return $this->hasOne(Regions::className(), ['id' => 'id_region']);
    }
    public function getCity() {
        return $this->hasOne(Cities::className(), ['id' => 'id_city']);
    }
}
