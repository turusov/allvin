<?php
/**
 * Created by PhpStorm.
 * User: turus
 * Date: 22.09.2016
 * Time: 9:47
 */

namespace frontend\models;
use yii\base\Model;


class FilterForm extends Model
{
    public $number;
    public $mark;
    public $model;
    public $modification;
    public $region;
    public $city;
    public $brand;

    public function attributeLabels()
    {
        return [
            'number' => 'Номер или название запчасти',
            'mark' => 'Марка автомобиля',
            'model' => 'Модель автомобиля',
            'modification' => 'Поколение',
            'region' => 'Регион',
            'city' => 'Город',
            'brand' => 'Производитель'
        ];
    }

    public function rules()
    {
        return [

            [['mark', 'model', 'region', 'city', 'brand'], 'integer'],
            [['number'], 'string', 'max' => 250],
        ];
    }

}