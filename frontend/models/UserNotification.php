<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_notification".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $message
 * @property integer $shop
 */
class UserNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'show'], 'integer'],
            [['message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'message' => 'Message',
            'show' => 'Shop',
        ];
    }
}
