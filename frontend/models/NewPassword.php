<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
class NewPassword extends \yii\db\ActiveRecord {
    public $old_password;
    public $new_password;
    public $confim_password;

    public function rules()
    {
        return [
            ['old_password','findPasswords'],
            [['new_password', 'confim_password'],'string','min'=>5],
            [['old_password', 'new_password', 'confim_password'], 'safe'],
            [['old_password', 'new_password', 'confim_password'], 'required'],
            ['new_password', 'compare', 'compareAttribute' => 'confim_password'],
            [['old_password', 'new_password', 'confim_password'], 'string', 'max' => 255],
            ['new_password', 'compare', 'compareAttribute' => 'old_password', 'operator' => '!='],
        ];
    }

    public function attributeLabels()
    {
        return [
            'old_password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'confim_password' => "Подтверждение"
        ];
    }
    public function findPasswords($attribute, $params){
        $user = User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();

        if(!Yii::$app->getSecurity()->validatePassword($this->old_password, $user->password_hash))
            $this->addError($attribute,'Старый пароль не совпадает');
    }
}