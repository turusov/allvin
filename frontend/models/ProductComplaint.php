<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product_complaint".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_complaint
 * @property integer $id_user
 * @property string $text
 */
class ProductComplaint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_complaint';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_complaint', 'text'],  'required'],
            [['id_product', 'id_complaint', 'id_user'], 'integer'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Запчасть',
            'id_complaint' => 'Тема жалобы',
            'id_user' => 'Пользователь',
            'text' => 'Текст жалобы',
        ];
    }
}
