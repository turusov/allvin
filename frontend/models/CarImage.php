<?php

namespace frontend\models;


use Yii;

/**
 * This is the model class for table "car_image".
 *
 * @property integer $id_car_image
 * @property integer $id_car_model
 * @property string $name
 */
class CarImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_car_model'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_image' => 'Id Car Image',
            'id_car_model' => 'Id Car Model',
            'name' => 'Name',
        ];
    }
}
