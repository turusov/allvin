<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "regions".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $title_ru
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'country_id'], 'integer'],
            [['title_ru'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'title_ru' => 'Title Ru',
        ];
    }
}
