<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $name
 * @property string $description
 * @property string $small_description
 * @property string $image
 * @property string $number
 * @property double $price
 * @property integer $availability
 * @property integer $active
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'number', 'max' => 9999999999],
            [['description'], 'string', 'min' => 10],
            [['name'], 'string', 'max' => 255, 'min' => 3],
            [['number'], 'string', 'max' => 50, 'min' => 3],
            [['id_user', 'availability', 'active', 'availability'], 'integer'],
            [['name', 'price', 'description', 'id_user', 'id_address', 'availability',  'active', 'date_create'],  'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'id_user' => 'Пользователь',
            'id_address' => 'Адрес',
            'name' => 'Наименование детали',
            'description' => 'Описание детали',
            'small_description' => 'Короткое описание',
            'image' => 'Фото',
            'number' => 'Номер автозапчасти',
            'price' => 'Цена',
            'availability' => 'В наличии',
            'active' => 'Active',
        ];
    }

    public function getMainImages() {
        return $this->hasOne(PartsImage::className(), ['id_product' => 'id'])->where(['main'=>1]);
    }
    public function getMarks() {
        return $this->hasOne(ProductMark::className(), ['id_product' => 'id']);
    }
    public function getModel() {
        return $this->hasOne(ProductModel::className(), ['id_product' => 'id']);
    }

     public function upload()
    {
        if ($this->validate()) {    //model->image !== null
            #$uploadedFile=CUploadedFile::getInstance($this,'image');

            $upload = '/uploads/products/';

            if(!$this->image !== null)  // check if uploaded file is set or not
            {
                $this->image->saveAs(Yii::getPathOfAlias('webroot').$upload.$this->image[0]);
            }
            exit;
            //$ext = substr(strrchr($model->file,'.'),1);
            $this->image->saveAs($upload.$this->image[0]->name);

            return true;
        } else {
            return false;
        }
    }
    public function getPartsById($id) {
        return Products::find()->select('name')->where(['id'=>$id])->one()['name'];
    }

    public function getAddress() {
        return $this->hasOne(UserAddress::className(), ['id' => 'id_address']);
    }
    
}
