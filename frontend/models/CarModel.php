<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "car_model".
 *
 * @property integer $id
 * @property integer $id_car_mark
 * @property string $name
 * @property integer $date_create
 * @property integer $date_update
 * @property integer $id_car_type
 * @property string $name_rus
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['id_car_mark', 'date_create', 'date_update', 'id_car_type'], 'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_car_mark' => 'Id Car Mark',
            'name' => 'Name',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'id_car_type' => 'Id Car Type',
            'name_rus' => 'Name Rus',
        ];
    }
    public function getCarGeneration() {
        return $this->hasMany(CarGeneration::className(), ['id_car_model' => 'id']);
    }
     public function getCarImage() {
        return $this->hasOne(CarImage::className(), ['id_car_model' => 'id']);
    }
}
