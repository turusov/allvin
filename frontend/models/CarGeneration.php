<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "car_generation".
 *
 * @property integer $id_car_generation
 * @property string $name
 * @property integer $id_car_model
 * @property string $year_begin
 * @property string $year_end
 * @property integer $date_create
 * @property integer $date_update
 * @property integer $id_car_type
 */
class CarGeneration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_generation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_car_model', 'date_create'], 'required'],
            [['id_car_model', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['name', 'year_begin', 'year_end'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_generation' => 'Id Car Generation',
            'name' => 'Name',
            'id_car_model' => 'Id Car Model',
            'year_begin' => 'Year Begin',
            'year_end' => 'Year End',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'id_car_type' => 'Id Car Type',
        ];
    }
}
