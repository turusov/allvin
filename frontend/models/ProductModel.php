<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product_model".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_model
 */
class ProductModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Номер автозапчасти',
            'id_model' => 'Модель автомобиля',
        ];
    }
    
    
    
}
