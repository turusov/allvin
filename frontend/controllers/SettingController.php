<?php
/**
 * Created by PhpStorm.
 * User: turus
 * Date: 27.12.2016
 * Time: 11:21
 */

namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use frontend\models\UserContacts;
use common\models\User;
use frontend\models\NewPassword;
use frontend\models\UserNotification;
class SettingController extends Controller {

    public $layout = "user";

    public function actionIndex(){
        $contactsModel = new UserContacts();
        $passwordModel = new NewPassword();
        $notificationModel = new UserNotification();
        $modeluser = User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();
        $notificationModel->id_user = Yii::$app->user->id;
        $notificationModel->show = 0;

        if($passwordModel->load(Yii::$app->request->post())){


            if($passwordModel->validate()){
                $modeluser->password = crypt (md5($_POST['NewPassword']['new_password']));
                $modeluser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($_POST['NewPassword']['new_password']);
                if($modeluser->update()){
                    $notificationModel->message = "Ваш пароль изменен";

                }else{
                    $notificationModel->message = 'Ошибка, повторите попытку позже';

                }
            }
            $notificationModel->update();
        }

        if ($contactsModel->find('id_user', Yii::$app->user->id)->one()) {
            $contactsView = $contactsModel->find('id_user', Yii::$app->user->id)->one();
        } else {
            $contactsView = $contactsModel;
        }
        if($contactsModel->load(Yii::$app->request->post())){



            $save = 1;
            $contactsView->attributes = Yii::$app->request->post()['UserContacts'];
            if (!$contactsView->id_user) {
                $contactsView->id_user =  Yii::$app->user->id;
                $save = 0;
            }

            if ($contactsView->validate()) {
                if ($save) {
                    $contactsView->update();
                } else {
                    $contactsView->save();
                }

                $notificationModel->message = "Контактные данные были изменены";
                $notificationModel->save();
            }

        }





        return $this->render('index',[
            'contactsModel' => $contactsView,
            'passwordModel' => $passwordModel
        ]);
    }
    public function actionPassword() {

        return $this->render('password');
    }

}