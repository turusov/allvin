<?php
/**
 * Created by PhpStorm.
 * User: turus
 * Date: 18.11.2016
 * Time: 17:04
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;

use frontend\models\User;
use frontend\models\UserDialog;
use frontend\models\UserMessage;

class MessageController  extends Controller {
    public $user;
    public $layout = 'user';
    public function init() {
        $userModel = new User();
        $this->user = $userModel->find(Yii::$app->user->id)->one();
        if (!Yii::$app->user->id) {
            $this->redirect('/login');
        }
    }
    public function actionIndex()
    {
        $provider = new ActiveDataProvider([
            'query' => UserDialog::find()->with(['dialog', 'user'])->distinct()->where([
                'id_user'=> Yii::$app->user->id,

            ]),
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('index', [
            'message' => $provider
        ]);
    }


    public function actionView($id)
    {

        $messageModel = new UserMessage();

        $messageQuery = $messageModel->find()->distinct()->where(['id_dialog'=> $id]);
        $provider = new ActiveDataProvider([
            'query' => $messageQuery,
            'sort'=> ['defaultOrder' => ['id'=>SORT_ASC]],
            'pagination' => [
                'pageSize' => 1000000000000,
            ],
        ]);

       $messages = $messageModel->find()->distinct()->where([
           'id_dialog'=> $id,
           'reading' => 0
       ])->all();
       foreach ($messages as $message) {

            if ($message['id_user'] !=Yii::$app->user->id) {

                $message->reading = 1;
                $message->update();
            }
        }


        if ($_POST['UserMessage']) {
            $messageModel->addMessage($id, Yii::$app->user->id, $_POST['UserMessage']['message']);
        }
        return $this->render('view', [
            'messages' => $provider,
            'messageModel' =>$messageModel
        ]);
    }
}