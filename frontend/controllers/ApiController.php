<?php
/**
 * Created by PhpStorm.
 * User: turus
 * Date: 28.12.2016
 * Time: 11:28
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

use frontend\models\UserContacts;
use frontend\models\Products;
use frontend\models\UserNotification;
use frontend\models\UserAddress;
use frontend\models\Cities;
use frontend\models\PartsImage;
use frontend\models\CategoriesProduct;
class ApiController  extends Controller {
    public function actionPhone() {
        $contactModel = new UserContacts();
        $productModel = new Products();
        $notificationModel = new UserNotification();
        if (Yii::$app->request->isAjax) {
            $userId = $productModel->find('id', $_POST['item_id'])->select('id_user')->one()['id_user'];
            $notificationModel->id_user = $userId;
            $phone = $contactModel->find($userId)->select('phone')->one()['phone'];
            if (!$phone) {
                echo json_encode('Не указан');
                $notificationModel->message = "Добавьте в настройках пользователя номер телефона";
                $notificationModel->show = 0;
                $notificationModel->save();
            } else {
                echo json_encode($phone);
            }

        }
        exit;
    }
    public function actionAddress() {
         $city = new Cities();
        $userAddressModel = new UserAddress();
        $city = $city->find()->select('title_ru')->where(['id' => $_POST['UserAddress']['id_city']])->one()['title_ru'];
        $tempStr = $city." ".$_POST['UserAddress']['address'];
        $position = json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=".$tempStr))->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
        $userAddressModel->attributes = $_POST['UserAddress'];
        $userAddressModel->save();
        echo "<option selected value='".$userAddressModel->id."'>".$userAddressModel->address."</option>";
    }
    public function actionUparts() {
        $fileName = 'file';
        $userFolder = 'id'.\Yii::$app->user->identity->id;
        $uploadPath = '/home/users/e/ellosse/domains/ellosse.myjino.ru/uploads/products/'.$userFolder;
        if (isset($_FILES[$fileName])) {
            if (!file_exists($uploadPath)) {
                mkdir($uploadPath, 0700);
            }

            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            $extension = explode (".", $file->name)[end(explode (".", $file->name))];

            $mime_types = array('image/png', 'image/jpeg', 'image/bmp');
            $size = getimagesize($file->tempName);
            if(!in_array($size['mime'], $mime_types)){
                header('HTTP/1.1 500 Internal Server Error');
                header('Content-type: text/plain');
                $msg = "Error: Bad MIME type.";
                exit($msg);
            }

            $file->name = uniqid() . '.' . $extension;
            if ($file->saveAs($uploadPath . '/' . $file->name)) {

            }
        }
        echo $this->renderAjax('/user/_form-img_parts', [
            'file' => $file,
            'userId' => \Yii::$app->user->identity->id
        ]);
    }
    public function actionSet(){
         $id_user = 17;
         
         
         $request = Yii::$app->request;
         $address = $request->get('address');
         
         $productModel = new Products();
         $cityModel = new Cities();
         $userAddressModel = new UserAddress();
         $imageModel = new PartsImage();
         $create = 1;
         $id_address = 0;
         $userAddressItem = $userAddressModel->find()->where('address= :address', [':address' => $address])->one();
         if($userAddressItem) {
            $create = 0;
            $id_address = $userAddressItem['id'];
         }   
         if ($create) { 
            $tempStr = $address;
            $position = json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=".$tempStr))->response->GeoObjectCollection->featureMember[0]->GeoObject;
            $city = $cityModel->find()->where('title_ru= :title_ru', [':title_ru' => $position->name])->one();
            $points = explode(" ", $position->Point->pos);
            $userAddressModel->id_user = $id_user;
            $userAddressModel->id_region = $city['region_id'];
            $userAddressModel->id_city = $city['id'];
            $userAddressModel->address = $address;
            $userAddressModel->map_x = $points[0];
            $userAddressModel->map_y = $points[1];
            if($userAddressModel->save()) {
                $userAddressModel->save();
                $id_address = $userAddressModel['id'];
            }
         }      
         $productModel->name = $request->get('name');
         $productModel->id_user = $id_user;
         $productModel->price = str_replace(" ","", str_replace("&nbsp;руб.","",$request->get('price')));
         $productModel->description = $request->get('description');
         $productModel->availability = 0;
         $productModel->id_address = $id_address;
         $productModel->active = 0;
         $productModel->date_create = date("Y-m-d H:i:s");
         if($productModel->validate()) {
            $productModel->save();
         }
        
        $url = $request->get('photo');
        if($url && $productModel['id']){
            $userFolder = 'id'.$id_user;
            $uploadPath = '/home/users/e/ellosse/domains/ellosse.myjino.ru/uploads/products/'.$userFolder;
            $name = uniqid().'.jpg';
            $path = $uploadPath . '/' .$name;
            file_put_contents($path, file_get_contents('https:'.$url));
            $imageModel->id_product = $productModel['id'];
            $imageModel->image = $name;
            $imageModel->main = 1;
            $imageModel->save();
        }
        $categoriesProductModel = new CategoriesProduct();
        $categoriesProductModel->id_product = $productModel['id'];
         $categoriesProductModel->id_categories = 30;
         $categoriesProductModel->save();
    
        
    }
}