<?php
namespace frontend\controllers;

use frontend\models\CarMark;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

use frontend\models\Category;
use frontend\models\CategoriesProduct;
use frontend\models\User;
use frontend\models\Products;
use frontend\models\ProductMark;
use frontend\models\ProductModel;
use frontend\models\ProductModification;
use frontend\models\UserAddress;
use frontend\models\PartsImage;
use frontend\models\PartsBrand;
use frontend\models\CarModel;
use frontend\models\UserReviews;
use frontend\models\UserFavorite;
use frontend\models\FilterForm;
use yii\db\Expression;
/**
 * Site controller
 */
class ShopController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }




 
    public function actionIndex($model = "", $mark = '', $modification = "", $url = "", $brand="") {


        $filterModel = new FilterForm();
        $productQuery = Products::find()->where(['active' => 1, 'availability'=> 1]);
        $request = Yii::$app->request;
        if ($request->get('FilterForm')) {
            $filterModel->attributes = $request->get('FilterForm');
        }



        $title = "Автозапчасти";
        $meta_title = "Автозапчасти";
        $description = "Автозапчасти";
        
        
        
        switch (Yii::$app->request->get('sort')){
            case 'price_desc':
                $productQuery->orderBy(['price' => SORT_DESC]);

                break;
            case 'price_asc':
                $productQuery->orderBy(['price' => SORT_ASC]);
                break;
            case 'name':
                $productQuery->orderBy(['name' => SORT_ASC]);
                break;
            case 'newer':
                $productQuery->orderBy(['date_create' => SORT_DESC]);
                break;
            case 'popular':
                break;
        }
        if ($url || $model || $brand) {

        $ids = [];
        $productsArray = [];


        if ($url) {

            $category = Category::find()->where(['url' => $url])->one();
            $productsArray = CategoriesProduct::find()->select('id_product')->where(['id_categories'=>$category->id])->all();
            $title = $category->name;
            $meta_title = $category->name;
            $description = $category->meta_description;
        }
        if ($model) {
            
             $title = "Автозапчасти для ".$mark." ".$model;
            $meta_title = "Автозапчасти для ".$mark." ".$model;
            $description = "Автозапчасти для ".$mark." ".$model;
        
            
            $modelID = CarModel::find()
                ->select('id')
                ->where('name=:name', [':name' => $model])
                ->one();
            $productsArray = ProductModel::find()
                ->select('id_product')
                ->where('id_model=:id_model', [':id_model' => $modelID['id']])
                ->all();

        }

        if ($brand) {
            $title = "Автозапчасти от бренда ".$brand;
            $meta_title = "Автозапчасти от бренда ".$brand;
            $description = "Автозапчасти от бренда ".$brand;
            
            $brandModel = new PartsBrand();
            $brandId = $brandModel->find()->where(['name' => $brand])->select('id')->one()['id'];


            $productQuery->andWhere(['id_brand' => $brandId]);
        }


            if ($url || $model) {
                foreach ($productsArray as $id) {
                    $ids[] = $id->id_product;
                }
                $productQuery->andWhere(['id' => $ids]);
            }
        }




        if ($filterModel) {
            $joinSql = '';
            if ($filterModel->number) {

                $productQuery->andWhere('number LIKE :number OR name LIKE :number');
                $productQuery->addParams([':number' => '%' . $filterModel->number . '%']);
            }
            if ($filterModel->mark) {

                $joinSql[] = 'marks';
                $productQuery->andWhere(['id_mark' => $filterModel->mark]);
            }
            if ($filterModel->model) {

                $joinSql[] = 'model';
                $productQuery->andWhere(['id_model' => $filterModel->model]);
            }
            if ($filterModel->brand) {

                $productQuery->andWhere(['id_brand' => $filterModel->brand]);
            }

            if ($joinSql)
                $productQuery->joinWith($joinSql);
        }

        $provider = new ActiveDataProvider([
            'query' => $productQuery,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);


        $model = new Category();
        return $this->render('index', [
            'meta_title' => $meta_title,
            'meta_description' => $description,
            'title' => $title,
            'dataProvider'=>$provider,
            'category'=>$model->getStructure()
        ]);
    }
  
    
    public function actionView($id) {

        $review = new UserReviews();
        if(\Yii::$app->user->identity->id) {
            $review = new UserReviews();
            $review->id_user = \Yii::$app->user->identity->id;
            $review->id_product = $id;
            $review->date = date('Y-d-m H:i:s');
            $review->save();
        }


        $product = Products::find()->where([    
            'id' => $id,
            'active' => 1, 
            'availability'=> 1
            
        ])->with(['marks', 'model'])->one();
        if (!$product) {
                   throw new \yii\web\NotFoundHttpException("Запчасть была удалена или еще не добавлена");
        }
        $product->reviews += 1;
        $product->update();

        $reviewCount = $review->find()->select('count(*)')->where(['id'=> $product->id])->scalar();
        $productImage = PartsImage::find()->where(['id_product' => $product->id])->all();
        $productMainImage = PartsImage::find()->where(['id_product' => $product->id, 'main' => 1])->one();
        $productUser = User::find()->where(['id'=>$product->id_user])->one();

        $markId =    ProductMark::find('id_product', $product->id)->one()['id_mark'];
        $modelId = ProductModel::find('id_product', $product->id)->one()['id_model'];
        $CarMark = CarMark::find('id_car_mark', $markId)->one();
        $CarModel = CarModel::find('id_car_model', $modelId)->one();
        $addressView = UserAddress::find('id', $product->id_address)->with(['regions', 'city' ])->one();
       // print_r($productUser->username);
       
       
       

       
        $model = new Category();
        return $this->render('view', [
            'product'=>$product,
            'category'=>$model->getStructure(),
            'images'=>$productImage,
            'mainImage'=>$productMainImage,
            'review' => $reviewCount,
            'user' => $productUser,
            'carMark' => $CarMark,
            'carModel' => $CarModel,
            'address' => $addressView
        ]);
    }

    public function actionWishlist()
    {
        $favoriteModel = new UserFavorite();
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            if ($data['item_id']) {
                $favorite = $favoriteModel->find()->where([
                    'id_user' => \Yii::$app->user->identity->id,
                    'id_product' => $data['item_id'],

                ])->one();

                if (!count($favorite) || !$favorite->active) {
                    $favoriteModel->id_user = \Yii::$app->user->identity->id;
                    $favoriteModel->id_product =  $data['item_id'];
                    $favoriteModel->active = 1;
                    $favoriteModel->save();
                    return "_create_";
                } else {
                    return '_double_';
                }
            } else {
                return '_invalid_id_';
            }

        }
        return '_error_';
    }

}