<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use frontend\models\Category;
use yii\data\ActiveDataProvider;

use frontend\models\Products;
use frontend\models\CategoriesProduct;

/**
 * Site controller
 */
class CategoryController extends Controller
{
    
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['Create, update, index'],
                'rules' => [
                    [
                        'actions' => ['Create, update, index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    
                ],
            ],
        ];
    }

    public function actionCreate() {
        $model = new Category();
        if ($_REQUEST['Category']) {
            $model->attributes = $_REQUEST['Category'];
            if($model->validate()) {
                $model->save();
            }
        }
        return $this->render('create', [
            'model'=>$model
        ]);
    }
        
    public function actionUpdate($id) {
        $modelCat = new Category();
        $model = Category::findOne($id);
        if ($_REQUEST['Category']) {
            $model->attributes = $_REQUEST['Category'];
            if($model->validate()) {
                $model->update();
            }
        }        
        return $this->render('create', [
            'model'=>$model,
        ]);
    }
    public function actionIndex() {

        $provider = new ActiveDataProvider([
            'query' => Category::find(),
            'pagination' => [
                'pageSize' => 80,
            ],
        ]);  
        $category = $provider->getModels();
        return $this->render('index', [
            'model'=>$category,
        ]);
    }
}