<?php
/**
 * Created by PhpStorm.
 * User: turusov
 * Date: 20.09.2016
 * Time: 13:47
 */

namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use frontend\models\PartsBrand;
use yii\filters\AccessControl;


class BrandController  extends Controller {

    public function actionGet() {
        $url = "http://www.port3.ru/avtozapchasty/brands";

        $data = file_get_contents($url);
        $re="/(?=\<div class=\"name\">)(<div[^>]*>(?:(?:(?!<\/div>)(?!<div[^>]*>).)*|(?1))+<\/div>)(?<=\<\/div>)/is";
        preg_match_all($re, $data, $match);
        unset($match[0][0]);


        foreach ($match[0] as $brand) {
            $brandModel = new PartsBrand();
            $brandModel->name = str_replace("</div>", "", str_replace("<div class=\"name\">", "", $brand));
            $brandModel->save();
        }
    }

    public function actionIndex() {



        $brandQuery = PartsBrand::find()->where('img <> ""');
        $provider = new ActiveDataProvider([
            'query' => $brandQuery,
            //'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => 36,
            ],
        ]);


        return $this->render('index', [
            'dataProvider' => $provider
        ]);
    }
}