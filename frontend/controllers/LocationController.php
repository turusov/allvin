<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use frontend\models\Cities;

/**
 * Site controller
 */
class LocationController extends Controller
{

    function actionAjaxcity() {
        $request = Yii::$app->request;
        $id_region = $request->post('id_region');
        $modelsView = Cities::find()
            ->where('region_id=:id_region', [':id_region'=>$id_region])
            ->all();
        $str = "";
        if($modelsView>0){
            foreach($modelsView as $region){
                $str.= "<option value='".$region->id."'>".$region->title_ru."</option>";
            }
        }
        else{
            echo "<option>-</option>";
        }

        echo $str;
    }
    
}
