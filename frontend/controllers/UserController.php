<?php

namespace frontend\controllers;

use frontend\models\UserMessage;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;


//Модели
use frontend\models\User;
use frontend\models\UserReviews;
use frontend\models\UserFavorite;
use frontend\models\Cities;
use frontend\models\UserAddress;
use frontend\models\Products;
use frontend\models\ProductMark;
use frontend\models\ProductModel;
use frontend\models\CategoriesProduct;
use frontend\models\PartsImage;
use frontend\models\UserNotification;
class UserController extends Controller {
    public $user;
    public $layout = 'user';
    public function init() {
        $userModel = new User();
        $this->user = $userModel->find()->where(['id'=>Yii::$app->user->id])->one();
        if (!Yii::$app->user->id) {
            $this->redirect('/login');
        }

    }

    public function actionIndex() {
        $reviewsQuery = UserReviews::find()->with('products')->where(['active' => 1, 'id_user'=> Yii::$app->user->id])->limit(8);

        $reviewsProvider = new ActiveDataProvider([
            'query' => $reviewsQuery,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'totalCount' => 8,
            'pagination' => [
                'pageSize' => 8,
            ]
        ]);

        $favoriteQuery = UserFavorite::find()->with('products')->where(['active' => 1, 'id_user'=> Yii::$app->user->id])->limit(8);

        $favoriteProvider = new ActiveDataProvider([
            'query' => $favoriteQuery,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'totalCount' => 8,
            'pagination' => [
                'pageSize' => 8,
            ]
        ]);

        $productQuery = Products::find()->where(['active' => 1, 'id_user'=> Yii::$app->user->id])->limit(8);

        $productProvider = new ActiveDataProvider([
            'query' => $productQuery,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'totalCount' => 8,
            'pagination' => [
                'pageSize' => 8,
            ]
        ]);

        return $this->render('index', [
            'user'=>$this->user,
            'reviews' => $reviewsProvider,
            'favorite' => $favoriteProvider,
            'product' => $productProvider,

        ]);
    }

    public function actionPassword() {
        $userModel = User::find()->where(['id'=>Yii::$app->user->id])->one();
        return $this->render('password', [
            'userModel'=>$userModel,

        ]);


    }


    public function actionCreate_address() {
        $city = new Cities();
        $userAddressModel = new UserAddress();
        $city = $city->find()->select('title_ru')->where(['id' => $_POST['UserAddress']['id_city']])->one()['title_ru'];
        $tempStr = $city." ".$_POST['UserAddress']['address'];
        $position = json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=".$tempStr))->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
        $userAddressModel->attributes = $_POST['UserAddress'];
        $userAddressModel->save();
        echo "<option selected value='".$userAddressModel->id."'>".$userAddressModel->address."</option>";
    }
    public function actionReviews(){
        $title = "История просмотров";
        $reviewsQuery = UserReviews::find()->with('products')->where(['active' => 1, 'id_user'=> Yii::$app->user->id]);
        $reviewsProvider = new ActiveDataProvider([
            'query' => $reviewsQuery,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('reviews', [
            'provider' => $reviewsProvider,
            'name' => $title
        ]);
    }
    public function actionParths(){

        $request = Yii::$app->request;
        if ($request->post('id')) {
            $product = Products::find()->where(['availability' => 1, 'id'=>$request->post('id'),'id_user'=> Yii::$app->user->id])->one();
            switch ($request->post('action')) {
                case 'hide':
                    $product->active = 0;
                    $product->save();
                    break;
                case 'show':
                    $product->active = 1;
                    $product->save();
                    break;
                case 'remove':
                    $product->delete();
                    break;
            }
        }
  
        $productQuery = Products::find()->where(['availability' => 1, 'id_user'=> Yii::$app->user->id])->limit(8);
        $productProvider = new ActiveDataProvider([
            'query' => $productQuery,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'totalCount' => 8,
            'pagination' => [
                'pageSize' => 8,
            ]
        ]);
        return $this->render('parths', [
            'provider' => $productProvider,
        ]);
    }
    public function actionFavorites(){
        $title = "Избранные товары";
        $request = Yii::$app->request;
        if ($request->post('id')) {
            $favorite = UserFavorite::find()->with('products')->where(['active' => 1, 'id' => $request->post('id'),'id_user'=> Yii::$app->user->id])->one();
            $favorite->active = 0;
            $favorite->update();
        }
        $provider = new ActiveDataProvider([
            'query' => UserFavorite::find()->with('products')->where(['active' => 1, 'id_user'=> Yii::$app->user->id]),
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('favorites', [
            'provider' => $provider,
            'name' => $title,
            'remove' => 1
        ]);
    }

    public function actionAddress()
    {
        $request = Yii::$app->request;
        if ($request->post('id')) {
            switch ($request->post('action')) {
                case 'main':
                    $address = UserAddress::find()->where(['active' => 1, 'main' => 0, 'id'=>$request->post('id'), 'id_user'=> Yii::$app->user->id])->one();
                    $addressOld = UserAddress::find()->where(['active' => 1, 'id_user'=> Yii::$app->user->id, 'main' => 1])->one();

                    if(isset($addressOld)){
                        $addressOld->main = 0;
                        $addressOld->update();
                    }
                    $address->main = 1;
                    $address->update();
                    break;
                case 'remove':
                    $address = UserAddress::find()->where(['active' => 1, 'main' => 0, 'id'=>$request->post('id'), 'id_user'=> Yii::$app->user->id])->one();
                    $address->active = 0;
                    $address->update();

                    break;
            }
        }
        $provider = new ActiveDataProvider([
            'query' => UserAddress::find()->with(['regions', 'city'])->where(['active' => 1, 'id_user'=> Yii::$app->user->id]),
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('address', [
            'address' => $provider
        ]);
    }
    public function actionAddresscreate() {
        $userAddressModel = new UserAddress;
        if($_POST) {
            $userAddressModel->attributes = $_POST['UserAddress'];
            $city = new Cities();
            $position = explode(" ",json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=".$city->find()->select('title_ru')->where(['id' => $userAddressModel->id_city])->one()['title_ru']." ".$userAddressModel->address))->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
      
            $userAddressModel->map_x = $position[0];
            $userAddressModel->map_y = $position[1];
            $userAddressModel->id_user = Yii::$app->user->id;
            $userAddressModel->active = 1;
            if ($userAddressModel->validate()) {
                $userAddressModel->save();
                $this->redirect('/user/address/');
            }

        }
        return $this->render('address_create', [
            'userAddress' => $userAddressModel
        ]);
    }



    public function actionAddresedit($id) {
        $userAddressModel = UserAddress::find()->where(['id'=>$id, 'id_user'=>Yii::$app->user->id, 'active'=>1])->one();
        if($_POST) {
            $userAddressModel->attributes = $_POST['UserAddress'];
            $city = new Cities();
            $position = explode(" ",json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=".$city->find()->select('title_ru')->where(['id' => $userAddressModel->id_city])->one()['title_ru']." ".$userAddressModel->address))->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
           
            
            $userAddressModel->map_x = $position[0];
            $userAddressModel->map_y = $position[1];
            if ($userAddressModel->validate()) {
              
                $userAddressModel->update();
                $this->redirect('/user/address/');
            }
        }
        return $this->render('address_edit', [
            'userAddress' => $userAddressModel
        ]);
    }

    public function actionCparts() {
        $productModel = new Products();

        $productMarkModel = new ProductMark();
        $productModelModel = new ProductModel();
        $userAddressModel = new UserAddress();
        $partsImageModel = new PartsImage();
        $category_prodModel  = new CategoriesProduct();
        if (\Yii::$app->request->post('Products')) {
            $productModel->attributes = \Yii::$app->request->post('Products');
            $productModel->id_user =  \Yii::$app->user->identity->id;
            $productModel->active = 1;
            $productModel->availability= 1;
            $productModel->date_create =  date("Y-m-d H:i:s");
            $productModel->save();

                    $main = 0;
                    if(isset($_POST['main'])){
                        foreach ($_POST['main'] as $key=>$m) {
                            
                            if ($m=='on') {
                                $main = $key;
                            }
                        }
                    }
                    if(isset($_POST['images'])) {
                        foreach ($_POST['images'] as $key => $image) {
                            $partsImageModel = new PartsImage();


                            if ($main == $key) {
                              
                                $partsImageModel->main = 1;
                            }
                            
                            $partsImageModel->id_product = $productModel->id;
                            $partsImageModel->image = $image;
                           
                            $partsImageModel->save();
                        }
                    }

                    $notificationModel = new UserNotification();
                    $notificationModel->id_user = \Yii::$app->user->identity->id;
                    $notificationModel->message = "Запись добавлена";
                    $notificationModel->show = 0;
                    $notificationModel->save();
                    
                    
                    $this->redirect('/parts/id'.$productModel['id']);
          
        
        }
        return $this->render('parts_create', [
            'product' => $productModel,
            'category_product' => $category_prodModel,
            'productMark' => $productMarkModel,
            'productModel' => $productModelModel,
            'userAddressModel' => $userAddressModel,
            'partsImageModel' => $partsImageModel
        ]);
    }

    public function actionUparts($id) {
        $productModel = Products::find()->where(['id'=> $id])->one();


        $productMarkModel = ProductMark::find(['id_product'=> $id])->one();
        if(!$productMarkModel) {
            $productMarkModel = new ProductMark();
        }            
            


        $productModelModel = ProductModel::find(['id_product'=> $id])->one();
         if(!$productModelModel) {
            $productModelModel = new ProductModel();
        }     
        $userAddressModel =  UserAddress::find()->where(['id_user'=> $productModel->id_user])->one();

        $categoryModel  = CategoriesProduct::find(['id_product'=> $id])->one();
        if(!$categoryModel) {
            $categoryModel = new CategoriesProduct();
        }   
        $partsImageModel =  PartsImage::find()->where(['id_product' => $id])->all();
        if ($_POST) {

       
            $productModel->attributes = $_POST['Products'];
            $productModel->date_edit = date("Y-m-d H:i:s");
            $productMarkModel->attributes = $_POST['ProductMark'];
            $productModelModel->attributes = $_POST['ProductModel'];
            $categoryModel->attributes = $_POST['CategoriesProduct'];
          
            if ($productModel->validate()) {

                foreach ($partsImageModel as $image) {

                    $image->delete();

                }
                $main = 0;

                if ($_POST['main'])
                foreach ($_POST['main'] as $key=>$m) {
                    if ($m=='on') {
                        $main = $key;
                    }
                }
                if ($_POST['images']) {
                   
                    foreach ($_POST['images'] as $key=>$image) {
                    $partsImageModel = new PartsImage();


                    if ($main == $key) {
                        $partsImageModel->main = 1;
                    }
                    $partsImageModel->id_product = $productModel->id;
                    $partsImageModel->image = $image;
                    
                   
                    
                    $partsImageModel->save();
                }
                }
                
                $productModel->save();
                $productMarkModel->save();
                $productModelModel->save();
                $categoryModel->save();
                $notificationModel = new UserNotification();
                $notificationModel->id_user = \Yii::$app->user->identity->id;
                $notificationModel->message = "Запись изменена";
                $notificationModel->show = 0;
                $notificationModel->save();
            }


        }
        $partsImageModel =  PartsImage::find()->where(['id_product' => $id])->all();



        return $this->render('parts_update', [
            'product' => $productModel,
            'category_product' => $categoryModel,
            'productMark' => $productMarkModel,
            'productModel' => $productModelModel,
            'userAddressModel' => $userAddressModel,
            'partsImageModel' => $partsImageModel
        ]);
    }

}