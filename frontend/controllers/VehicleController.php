<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\CarMark;
use frontend\models\CarModel;
use frontend\models\CarImage;
use frontend\models\CarGeneration;
use frontend\models\Products;


use yii\data\ActiveDataProvider;

class VehicleController extends Controller {
    
    public function actionMarks() {
        $marksView = CarMark::find()
        ->andWhere('logo <> :logo',[':logo' => ''])
        ->orderBy(['popular' => SORT_DESC])
        ->all();
        return $this->render('marks', ['marks'=>$marksView]);
    }
     public function actionModels($mark) {
        $marksView = CarMark::find()
        ->select('id_car_mark, name')
        ->where('name = :name',[':name' => $mark])
        ->one();
        $modelsView = CarModel::find()       
        ->with('carGeneration')
        ->with('carImage')
        ->where('id_car_mark=:id_mark', [':id_mark'=>$marksView['id_car_mark']])
        ->all();
        foreach($modelsView as $item) {
            if (!$item['carImage']['name']) {
                $image = new CarImage();	//Create a new author
            $image->id_car_model = $item['id'];
            $image->name = $item['name'].'.jpg';
            $image->save();
            $count = 4;
            if ($_GET['img']) {
                $count = $_GET['img'];
            }
            $this->getImageRambler($marksView['name']." ".$item['name'].'"', $item['name'], "model/".$marksView['name']."/", $count ,'jpg');
            }  
        }
        return $this->render('models', [
            'name' => $marksView['name'],
            'models'=>$modelsView
        ]);
    }
    public function actionAjax_models(){
        $request = Yii::$app->request;
        $idMarks = $request->post('marks');
        $modelsView = CarModel::find()       
        ->where('id_car_mark=:id_mark', [':id_mark'=>$idMarks])
        ->all();
        $str = "";
        if($modelsView>0){
            foreach($modelsView as $model){
                $str.= "<option value='".$model->id."'>".$model->name."</option>";
            }
        }
        else{
            echo "<option>-</option>";
        }
        
        echo $str;
        
    }
    
    public function actionRemove($mark, $id) {
         $modelsView = CarImage::deleteAll('id_car_model = :id', [':id' => $id]);
         $this->redirect('/marks/'.$mark.'?img='.($_GET['img']));  
    }
     public function  getImageRambler($search, $name, $path = "marks/", $count = 1, $format = 'png', $params = "", $url=""){
        $url="http://images.rambler.ru/search?query=".urlencode(strtolower($search)).$params;
      
        $data=file_get_contents($url);
       $temp = substr($data, strpos($data,'class="js-serp-images__data"'));
        $nf=substr($temp, 0, strpos($temp,'<script>'));
        preg_match_all("/www.([^\'\"]+).(?:jp(?:e?g|e|2)|gif|png|tiff?|bmp|ico)/i", $nf, $output_array);
        $tempImageArray = Array();
        for ($i=0; $i<count($output_array[0]); $i++) {
            if (strripos($output_array[0][$i], ".".$format)) {
                 $tempImageArray[] = str_replace("\/", "/",$output_array[0][$i]);
            }
        }
        $tempImageArray = array_unique($tempImageArray);
        $file = str_replace("\/", "/", $tempImageArray[$count]);
           
            $image_formate = array_pop(explode('.', $file));
            if (!file_exists('./uploads/'.$path))
            mkdir('./uploads/'.$path, 0755, true);
            $name = str_replace('/', '_', $name);
            $newfile = './uploads/'.$path.$name.'.'.$format;
            
            $ch = curl_init($file);
            $fp = fopen($newfile, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
        return $name.'.'.$format;
       
    }
}