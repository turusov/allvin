<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'thumbnail' => [
            'class' => 'sadovojav\image\Thumbnail',
        ],
        'easyImage' => [
            'class' => 'cliff363825\image\EasyImage',
            'driver' => 'GD',
            'quality' => 100,
            'cachePath' => '/easyimage/',
            'cacheTime' => 2592000,
            'retinaSupport' => false,
            'basePath' => '@webroot',
            'baseUrl' => '@web',
        ],
        'cache' => [
            'class' => 'yii\caching\ApcCache',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
      
          'urlManager' => [
            'enablePrettyUrl' 	=> true,
            'showScriptName' => false,
            
            'rules' => [
                '' => 'site/index',
				'/'=>'site/',
                '/models' => 'vehicle/ajax_models',
                '/city' => 'location/ajaxcity',
                '/login'=>'site/login/',
                '/registration'=>'site/signup/',
                
                '/parts'=>'shop/index/',
                '/parts/id<id:\d+>'=>'shop/view/',
                'brand' => 'brand/index',
                
                '/user/parths/add'=>'user/cparts/',
                '/user/parths/edit/id<id>'=>'user/uparts/',

                'marks'=>'vehicle/marks',  
                'marks/<mark>'=>'vehicle/models',

                'marks/<mark>/<model>/<modification>'=>'shop/index',
                'marks/<mark>/<model>/'=>'shop/index',
                'brand/<brand>/'=>'shop/index',

                '/parts/phone' => 'api/phone',
                'marks/<mark>/remove<id:\d+>'=>'vehicle/remove',
                //Категории
                'parts/<url>'=>'shop/index',

                'categories/add'=>'category/create',
                'categories/update/<id>'=>'category/update',
                //Профиль
                'user/setting' => 'user/setting',
                'user/address/ajax' => 'user/create_address',
                'user/address/create' => 'user/addresscreate',
                '/user/address/edit/id<id>'=> 'user/addresedit',
                '/user/message/<id>'=> 'message/view',
                '/user/message' => 'message/index',

                'user/setting' => 'setting/index',
                'user/setting/password' => 'setting/password',
                'user/setting/contacts' => 'setting/contacts',

            ],
        ],
       
    ],
    'params' => $params,
];
