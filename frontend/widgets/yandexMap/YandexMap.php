<?php
namespace frontend\widgets\yandexMap;
use frontend\models\Products;
use yii\helpers\Html;
use yii\web\View;
use Yii;

class YandexMap extends \yii\bootstrap\Widget {
    public $item;
    public function run() {
        $parthsModel = new Products();
        if ($this->item){
            $items = $parthsModel->find()->where(['id'=> $this->item])->with('address')->one();
            
            if ($items['address']['map_x'] && $items['address']['map_y'])
            $point = $this->render('item', [
                'item' => $items
            ]);
        } else {
            $items = $parthsModel->find()->where([
                'availability'=> 1,
                'active' => 1
            ])->with('address')->all();

         

            $point = $this->render('items', [
                'items' => $items
            ]);

        }
        
        
        
        if ($point)
        return $this->render('map', [
            'placholder' => $point,
        ]);

    }
}