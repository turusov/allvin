navigator.geolocation.getCurrentPosition(function(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
   
    var myMap = new ymaps.Map("map", {
            center: [latitude, longitude],
            zoom: 9
    })
    <?foreach ($items as $key=>$item):?>
   
     var name = "<?=str_replace("\n", "", $item['name']);?>";
    myPlacemark<?=$key?> = new ymaps.Placemark([<?=$item['address']['map_y']?>, <?=$item['address']['map_x']?>], {
        balloonContent: '<a href="/parts/id<?=$item['id']?>">'+name+'</a>'
    });
    <?endforeach;?>
    
    myMap.geoObjects
    <?foreach ($items as $key=>$item):?>
    .add(myPlacemark<?=$key?>)
    <?endforeach;?>
    
});

