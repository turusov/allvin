<?php


namespace frontend\widgets\notificationWidget;
use Yii;
use frontend\models\UserNotification;

class NotificationWidget  extends \yii\bootstrap\Widget {
    public $id_user = 7;
    public function run() {
        $notifications = UserNotification::find()->where(['id_user'=> Yii::$app->user->id, 'show'=>0])->all();
        foreach ($notifications as $notification) {
            $item = UserNotification::findOne($notification->id);
            $item->show = 1;
            $item->update();
        }
        return $this->render('index', [
            'notifications' => $notifications
        ]);
    }
}