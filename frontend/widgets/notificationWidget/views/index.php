
<?php
foreach ($notifications as $notification) :
$script = <<< JS

    _toastr("$notification->message","top-right","info",false);

JS;
$this->registerJs($script, yii\web\View::POS_READY);
endforeach;
?>
