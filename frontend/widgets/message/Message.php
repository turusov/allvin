<?php
namespace frontend\widgets\Message;
use yii\helpers\Html;
use yii\web\View;
use Yii;
use frontend\models\UserMessage;
use frontend\models\Products;
use frontend\models\UserDialog;
use frontend\models\Dialog;
use frontend\models\UserNotification;
class Message extends \yii\bootstrap\Widget {

    public $id_product;
    public function run() {
        $messageModel = new UserMessage();
        $notificationModel = new UserNotification ();
        $product = Products::find(['id'=>$this->id_product])->one();
        $request = Yii::$app->request;
        if ($request->post('UserMessage')) {
            $allDialog = array_unique (UserDialog::find(['id_user'=>  \Yii::$app->user->identity->id])->select('id_dialog')->asArray()->all());


            $dialog = Dialog::find()->where(['id'=>$allDialog , 'id_product'=>$this->id_product])->asArray()->one();



            if (empty($dialog)) {
                $messageModel->create_dialog(\Yii::$app->user->identity->id, $this->id_product, $request->post('UserMessage')['message']);
            } else {
                $messageModel->addMessage($dialog['id'], \Yii::$app->user->identity->id, $request->post('UserMessage')['message']);
            }
            $notificationModel->id_user = \Yii::$app->user->identity->id;
            $notificationModel->message = 'Сообщение отправлено';
            $notificationModel->show = 0;
            $notificationModel->save();
        }
        return $this->render('message', [
            'messageModel'=>$messageModel,

        ]);
    }
}