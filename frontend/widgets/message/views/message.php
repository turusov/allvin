<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<?if (($user != Yii::$app->user->id) && Yii::$app->user->id):?>
<button data-toggle="modal" data-target="#message" class="btn btn-primary btn-lg btn-block size-15"><i class="fa fa-pencil"></i> Написать</button>

<div class="modal fade bs-example-modal-lg" tabindex="-1" id="message" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- header modal -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myLargeModalLabel">Написать сообщение</h4>
			</div>

			<!-- body modal -->
			<div class="modal-body">
				<?php $form = ActiveForm::begin([
						'enableAjaxValidation' => false,
						'enableClientValidation' => true,

				]); ?>
				<?= $form->field($messageModel, 'message')->textarea()?>

				<?= Html::submitButton('Отправить', ['class' => 'btn btn-info']) ?>
				<?php ActiveForm::end() ?>
			</div>

		</div>
	</div>
</div>

<? elseif(($user != Yii::$app->user->id) && !Yii::$app->user->id):?>
    <a href="/login" class="btn btn-primary btn-lg btn-block size-15"><i class="fa fa-pencil"></i> Написать</a>
<?endif;?>

