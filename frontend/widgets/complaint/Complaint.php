<?php

namespace frontend\widgets\Complaint;

use Yii;

use frontend\models\ProductComplaint;

class Complaint  extends \yii\bootstrap\Widget {
    public $id_product;
    public function run() {
        $complaintModel = new ProductComplaint();



        if (Yii::$app->request->isAjax) {
            $complaintModel->attributes = $_POST['ProductComplaint'];
            $complaintModel->id_product = $this->id_product;
            $complaintModel->id_user = \Yii::$app->user->identity->id;
            if ($complaintModel->validate()) {
                $complaintModel->save();
            }

        }


        return $this->render('popup', [
            'complaintModel' => $complaintModel
        ]);

    }

}