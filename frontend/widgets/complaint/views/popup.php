<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="modal fade bs-example-modal-lg" id="complaint" tabindex="-1" role="dialog" aria-labelledby="Жалоба на объявление" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Жалоба на объявление</h4>
            </div>

            <!-- body modal -->
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,

                ]); ?>
                <?= $form->field($complaintModel, 'id_complaint')->dropDownList([

                    1 => 'Товар продан',
                    2 => 'Неверная цена',
                    3 => 'Не дозвониться',
                    4 => 'Это не частное лицо',
                    5 => 'Контакты и ссылки в описании',
                    6 => 'Другая причина',
                    7 => 'Мошенничество с деньгами'
                ], [
                    'prompt' => 'Тема жалобы'
                ])->label(false);
                ?>
                <?= $form->field($complaintModel, 'text')->textArea() ?>

                <?= Html::submitButton('Отправить', ['class' => 'btn btn-info']) ?>
                <?php ActiveForm::end() ?>

            </div>

        </div>
    </div>
</div>
<?
$script = <<< JS

    $(function () {
        $('body').on('beforeSubmit', 'form#w4', function () {
            var form = $(this);
            // return false if form still have some validation errors
            if (form.find('.has-error').length)
            {
                return false;
            }
            $.ajax({
                type   : 'post',
                data   : form.serialize(),
            success: function (response)
            {
            	$('#complaint').find('textarea').val("");
            	$('#complaint').find('select').val("");
            	$('#complaint').modal("hide");
            	_toastr("Ваша жалоба уже обрабатывается!","top-right","warning",false);
            	$('#complaint').remove();


            },
            error  : function ()
            {
               	_toastr("Ошибка. Пожалуйста повторите попытку позже.","top-right","error",false);
            }
            });
            return false;
         });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

