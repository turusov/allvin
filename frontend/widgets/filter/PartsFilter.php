<?php

namespace frontend\widgets\filter;

use yii\helpers\Html;
use yii\web\View;
use Yii;

use frontend\models\FilterForm;
use frontend\models\PartsBrand;


class PartsFilter extends \yii\bootstrap\Widget {

    public $full;
    public function run() {
        $filterModel = new FilterForm;
        $filterModel->attributes = $_GET['FilterForm'];
        return $this->render('form', [
            'filter' => $filterModel,
        ]);

    }


}