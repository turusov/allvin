<?
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use frontend\models\Regions;
    use frontend\models\Cities;
    use frontend\models\CarMark;
    use frontend\models\CarModel;
    use frontend\models\PartsBrand;
?>

<?php $form = ActiveForm::begin([
    'id' => 'order-search-form',
    'method' => 'get',
    'action' => '/parts',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'margin-0'
    ]
]) ?>
    <div class="row">
        <div class="col-lg-11 col-xs-9">
            <?=$form->field($filter, 'number',['template' => '{input}'])->textInput(['placeholder' => 'Номер или наименование запчасти']) ?>
        </div>
        <div class="col-lg-1 col-xs-2">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="col-md-12">
            <a href="javascript:;" class="pull-right" onclick="jQuery('#pre-0').slideToggle(); ">Расширенный поиск</a>
        </div>

    </div>

    <div id="pre-0" class="text-left noradius text-danger softhide" <?if ($_GET['FilterForm']):?>style="display: block;"<?endif;?>>
        <div class="row">
            <div class="col-md-12">
                <?=$form->field($filter, 'brand', ['inputOptions'=>['encode'=>false]])->dropdownList(
                    PartsBrand::find()->select(['name', 'id'])->indexBy('id')->column(), array('prompt'=>'Выберите бренд'))->label(false);
                ?>
            </div>
            <div class="col-md-6">
                <?=$form->field($filter, 'mark', ['inputOptions'=>['encode'=>false]])->dropdownList(
                    CarMark::find()->select(['name', 'id_car_mark'])->indexBy('id_car_mark')->column(), array('prompt'=>'Марка автомобиля'))->label(false);
                ?>

            </div>
            <div class="col-md-6">
                           <? if(!$filter->mark):?>
                    <?=$form->field($filter, 'model')->dropdownList(
                        [], array('prompt'=>'Выберите модель автомобиля...'))->label(false);
                    ?>
                <?else:?>
                    <?=$form->field($filter, 'model')->dropdownList(
                        CarModel::find()->select(['name', 'id'])->where(['id_car_mark' => $filter->mark])->orderBy('name')->indexBy('id')->column(), array('prompt'=>'Выберите модель автомобиля...'))->label(false);
                    ?>

                <?endif;?>
            </div>

        </div>


    </div>

<?php ActiveForm::end() ?>



<?php
/**
 * Связанные списки
 */
$script_js = <<< JS


jQuery('#filterform-mark.form-control').change(function(){

    updateModel($(this).val());
    return false;
});
function updateModel(mark) {

   var first = $('#filterform-model option:first-child');
    $('#filterform-model option').remove();
    $('#filterform-model').append(first);

    $.ajax({
        url: '/models',
        type: "POST",
        data: {
            'marks':mark,
        },
        success: function (data) {

            $('#filterform-model').append(data);
        }
    });

}

jQuery('#filterform-region.form-control').change(function(){

    getCity($(this).val());
});
function getCity (region) {

     var first = $('#filterform-city option:first-child');
    $('#filterform-city option').remove();
    $('#filterform-city').append(first);


    $.ajax({
        url: '/city',
        type: "POST",
        data: {
            'id_region': region,
        },
        success: function (data) {
            $('#filterform-city ').append(data);
        }
    });
    return false;
};


JS;
$this->registerJs($script_js, yii\web\View::POS_READY);
?>