<?php
namespace frontend\widgets\userBlock;
use Yii;
use frontend\models\User;

use frontend\models\UserMessage;
use frontend\models\UserDialog;
class userBlock extends \yii\bootstrap\Widget {

    public function run() {
         $userModel = new User();
        $dialogModel = new UserDialog();
        $messagesModel = new UserMessage();
        $userDialog = $dialogModel->find()->select('id_dialog')->where([
            'id_user'=>Yii::$app->user->id,
        ])->asArray()->all();
        
        $userView = $userModel->find()->where(['id'=>Yii::$app->user->id])->one();
    
       $messages = $messagesModel->find()->where([
           'id_dialog' => $userDialog,
           'reading' => 0,

       ])->all();
        $count = 0;
        foreach ($messages as $message) {
            if ($message['id_user'] != Yii::$app->user->id) {
                $count++;
            }
        }
        return $this->render('user', [
            'count' => $count,
            'userView' => $userView
        ]);
    }
}