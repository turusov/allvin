<ul class="pull-right nav nav-pills nav-second-main">
    <? if(!Yii::$app->user->identity):?>
        <li class="">
            <a href="/login">
                <i class="fa fa-user"></i> <span class="hidden-xs">Авторизация</span>
            </a>
        </li>
        <li class="">
            <a href="/registration">
                <i class="fa fa-plus"></i> <span class="hidden-xs">Регистрация</span>
            </a>
        </li>
    <?else:?>
        <li>
            <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">
            <?if($count):?>
            <span class="badge badge-aqua btn-xs badge-corner"><?=$count?></span>
            <?endif;?>
          
       
            <i class="fa fa-user"></i> <span class="hidden-xs">Добро пожаловать <?=$userView['username']?></span></a>
            <ul class="dropdown-menu pull-right">
                <li><a tabindex="-1" href="/user"><i class="fa fa-user"></i> Профиль</a></li>
                <li><a class="txt-success" href="/user/parths/add"><i class="fa fa-plus-circle"></i> <span class="">Добавить запчасть</span></a></li>
                <li><a tabindex="-1" href="/user/message"><i class="fa  fa-envelope"></i> Сообщения </a></li>
                <li><a tabindex="-1" href="/user/favorites"><i class="fa fa-bookmark"></i> Закладки</a></li>
                <li><a tabindex="-1" href="/user/setting"><i class="fa fa-cog"></i> Настройки</a></li>
        <li><a tabindex="-1" href="/site/logout" data-method="post"><i class="fa fa-sign-out"></i> Выйти</a></li>
            </ul>
        </li>
    <?endif;?>
</ul>