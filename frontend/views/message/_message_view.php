<?

use frontend\models\User;

?>

    <div class="clearfix margin-bottom-20">
        <h4 class="size-15 nomargin noborder nopadding bold"><?=User::getUserName($model['id_user'])?></h4>
        <span class="size-13 text-muted">
            <?=$model['message']?>
            <span class="text-success size-11"><?=$model['date_create']?></span>
        </span>
    </div>
