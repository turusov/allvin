<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title ='Ваши сообщения';
?>
<section>
    <div class="container">
        <h1 class="size-20">Сообщения</h1>
        <div class="box-inner">
            <h3>
                Ваши сообщения
            </h3>
            <div class="slimscroll full-height" data-always-visible="true" data-size="5px" data-position="right" data-opacity="0.4" disable-body-scroll="false">
                <?=ListView::widget(['dataProvider' => $message, 'itemView' => '_message',  'layout' => "{pager}\n{items}",]);?>
            </div>
        </div>
    </div>
</section>

