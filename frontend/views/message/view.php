<?
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title ='Диалог';
?>
<section>
    <div class="container">
        <div class="box-inner">
            <h3>
                Диалог
            </h3>
            <div class="full-height-two slimscroll" data-always-visible="true" data-start="bottom" data-position="right" data-opacity="0.4" disable-body-scroll="true">
                <?=ListView::widget(['dataProvider' => $messages, 'itemView' => '_message_view',  'layout' => "{items}",]);?>

            </div>
        </div>
        <div class="box-footer">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,

            ]); ?>

            <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($messageModel, 'message')->textArea(['rows' => '6']) ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-reveal btn-info']) ?>
                    </div>
                </div>

            <?php ActiveForm::end() ?>

        </div>
    </div>
</section>



