<?
    use frontend\models\Products;
    use frontend\models\UserDialog;
    use frontend\models\UserMessage;
    $message = UserMessage::getLast($model['id_dialog']);


?>
<a href="/user/message/<?=$model['dialog']->id?>">
    <div class="clearfix margin-bottom-20">
        <?if(!$message['reading'] && $message['id_user'] !=Yii::$app->user->id):?><span class="label label-success pull-right">Новое</span><?endif;?>
        <h4 class="size-15 nomargin noborder nopadding bold"><?=Products::getPartsById($model['dialog']->id_product);?>   <span class="text-success size-11"><?=$message['date_create']?></span></h4>
        <span class="size-13 text-muted">
            <?=$message['message']?>
        </span>
    </div>
</a>