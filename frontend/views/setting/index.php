<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title ='Ваши запчасти';
?>
<section>
    <div class="container">
        <h1 class="size-20">Настройки</h1>

            <div class="clearfix ">
                <h2 class="owl-featured noborder"><strong>Контакты</strong></h2>
                <?php echo $this->render('_form_contacts', [
                    'contactsModel' => $contactsModel
                ]); ?>
            </div>
            <hr>
            <div class="clearfix ">
                <h2 class="owl-featured noborder"><strong>Пароль</strong></h2>
                <?php echo $this->render('_form_password', [
                    'passwordModel' => $passwordModel
                ]); ?>
            </div>
            <hr>
            <div class="clearfix ">
                <h2 class="owl-featured noborder"><strong>Адрес</strong></h2>
                <a href="/user/address" class="btn btn-primary">Управление адресами</a>
            </div>
             <br class="clearfix ">





    </div>
</section>
