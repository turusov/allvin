<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>


<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,

]); ?>
<?= $form->field($passwordModel, 'old_password')->passwordInput()->label('Старый пароль'); ?>
<?= $form->field($passwordModel, 'new_password')->passwordInput()->label('Новый пароль'); ?>
<?= $form->field($passwordModel, 'confim_password')->passwordInput()->label('Подтверждение нового пароля'); ?>
<?= Html::submitButton('Изменить', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end() ?>