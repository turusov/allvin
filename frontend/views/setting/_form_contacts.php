<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use frontend\models\Category;
?>


<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,

]); ?>
<?= $form->field($contactsModel, 'phone')->textInput(['class'=> 'form-control masked','data-format'=>'+9 (999) 999-9999', 'data-placeholder'=> "X", 'placeholder'=> 'Введите номер телефона']) ?>

<?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end() ?>