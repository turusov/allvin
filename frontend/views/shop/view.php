<?
use yii\widgets\Menu;
use frontend\widgets\yandexMap\YandexMap;
use frontend\widgets\message\Message;
use frontend\widgets\complaint\Complaint;

$this->title = $product->name;
$this->registerMetaTag(['name' => 'description', 'content' => 'AllVinExpress '.$product->name]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $product->name.', AllVinExpress, AllVin, AllVin Express, All, Vin, Express, All-Vin, покупка автозапчастей, автозапчасти, найти запчасть, продать автозапчасть, купить автозапчасть, продать']);


?>
<meta property="og:title" content="AllVin <?= $product->name ?>" />
<meta property="og:description" content="<?=strip_tags($product->description) ?>" />
<meta property="og:type" content="product" />
<meta property="og:url" content="/parts/id<?=$product['id']?>" />
<meta property="og:image" content="<?if ($mainImage->image):?>/uploads/products/id<?= $product->id_user?>/<?= $mainImage->image?><?else:?>/template/images/noimg.png<?endif;?>" />
<section class="padding-xs" itemscope itemtype="http://schema.org/Product">
    <div class="container ">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="side-nav margin-bottom-60">
                    <div class="side-nav-head">
                        <button class="fa fa-bars"></button>
                        <h4>Категории</h4>
                    </div>
                    <?= Menu::widget([
                        'items' => $category,
                        'options' => ['class' => 'list-group-item'],
                        'submenuTemplate' => "\n	<ul>\n{items}\n</ul>\n",
                        'linkTemplate' => '<a class="dropdown-toggle" href="{url}">{label}</a>',
                        'itemOptions' => array('class' => 'list-group-item'),
                        'activateParents' => true,
                        'encodeLabels' => false,
                        'options' => [
                            'class' => 'list-group list-group-bordered list-group-noicon',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9">

                <h1 itemprop="name" class="blog-post-title"><?= $product->name ?></h1>
                <? if (($product->id_user != \Yii::$app->user->identity->id)):?>
                    <? if (\Yii::$app->user->identity->id):?>
                    <div class="pull-right">
                        <!-- replace data-item-id width the real item ID - used by js/view/demo.shop.js -->
                        <a class="btn btn-default add-wishlist" href="#" data-item-id="<?=$product->id;?>" data-toggle="tooltip" title="" data-original-title="Добавить в закладки"><i class="fa fa-heart nopadding"></i></a>
                    </div>
                    <?endif;?>
                <? endif;?>
                <ul class="blog-post-info list-inline margin-top-10">
                    <li>
                            <i class="fa fa-clock-o"></i>
                            <span class="font-lato"> <?=date_format(new DateTime($product->date_create), "d-m-Y")?></span>
                    </li>
                    <li>
                            <i class="fa fa-eye"></i>
                            <span class="font-lato"> <?=$product->reviews;?> просмотров</span>
                    </li>
                    <li>
                            <i class="fa fa-star-o"></i>
                            <span class="font-lato"> <?=$review;?> добавили в избранное</span>
                    </li>
                    <li>
                        <i class="fa fa-car"></i>
                        <a class="category" href="/marks/<?=$carMark->name?>">
                            <span class="font-lato"><?=$carMark->name?></span>
                        </a>
                        <a class="category" href="/marks/<?=$carMark->name?>/<?=$carModel->name?>">
                            <span class="font-lato"><?=$carModel->name?></span>
                        </a>
                    </li>
                    <li>
                        <i class="fa fa-user"></i>
                        <span itemprop="seller" class="font-lato"><?=$user->username?></span>
                    </li>
                </ul>

                <div class="row">
                    <div class="col-lg-6 col-sm-12 m-top-20">
                        <div class="thumbnail relative margin-bottom-3">
                            <figure id="zoom-primary" class="zoom" data-mode="grab">
                                <a class="lightbox bottom-right" href="<?if ($mainImage->image):?>/uploads/products/id<?= $product->id_user?>/<?= $mainImage->image?><?else:?>/template/images/noimg.png<?endif;?>" data-plugin-options='{"type":"image"}'><i class="glyphicon glyphicon-search"></i></a>
                                <img class="img-responsive" src="<?if ($mainImage->image):?>/uploads/products/id<?= $product->id_user?>/<?= $mainImage->image?><?else:?>/template/images/noimg.png<?endif;?>" width="1200" height="1500" alt="This is the product title" />
                            </figure>

                        </div>
                        <div data-for="zoom-primary" class="zoom-more owl-carousel owl-padding-3 featured" data-plugin-options='{"singleItem": false, "autoPlay": false, "navigation": true, "pagination": false}'>
                            <?php foreach($images as $image):?>
                            <a class="thumbnail active" href="/uploads/products/id<?= $product->id_user?>/<?=$image->image;?>">
                                <img src="/uploads/products/id<?= $product->id_user?>/<?=$image->image;?>" height="50" alt="" />
                            </a>
                            <?endforeach;?>

                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-4">
                        <span class="clearfix" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                         <meta itemprop="priceCurrency" content="RUB" />
                            <span itemprop="price" class="pull-right size-20"><?= $product->price ?> руб.</span>
                            <strong class="pull-left size-20">Цена:</strong>
                        </span>
                       
                        <hr class="margin-0">
                        <br>

                        <? if ($product->id_user != \Yii::$app->user->identity->id):?>
                        <div class="row">
                            <div class="col-md-12 address margin-bottom-20">
                                <span class="block size-15"><strong><i class="fa fa-map-marker"></i> Адрес:</strong> <?=$address['regions']['title_ru']?> <?=$address['city']['title_ru']?>   <?=$address['address']?></span>
                            </div>
                            <div class="col-md-12 col-phone">
                                <canvas style="display:none;" id='canvas-phone'></canvas>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="show-phone btn btn-primary btn-lg btn-block size-15"  data-item-id="<?=$product->id;?>"><i class="fa fa-phone"></i>Показать телефон</button>
                            </div>
                            <div class="col-md-6">
                                <?=Message::widget([
                                    'id_product' => $product['id']
                                ]); ?>
                            </div>
                        </div>

                        <hr>
                        <? endif;?>


                        <div class="clearfix">
                            <span class="pull-left margin-top-6 bold hidden-xs">
									Поделиться:
								</span>
                            <div class="pull-right">

                                <a href="#" class="goodshare social-icon social-icon-sm social-icon-transparent social-vk" data-type="vk" data-toggle="tooltip" data-placement="top" title="VK">
                                    <i class="icon-vk"></i>
                                    <i class="icon-vk"></i>
                                </a>
                                <a href="#" class="goodshare social-icon social-icon-sm social-icon-transparent social-facebook" data-type="fb" data-toggle="tooltip" data-placement="top" title="Facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="goodshare social-icon social-icon-sm social-icon-transparent social-gplus" data-type="gp" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google plus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                                <a href="#" class="goodshare social-icon social-icon-sm social-icon-transparent social-twitter" data-type="tw" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="goodshare social-icon social-icon-sm social-icon-transparent social-linkedin" data-type="li" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>


                            </div>


                        </div>
                        <hr>
                        <div class="pull-right">

                            <a href="#" data-toggle="modal" data-target="#complaint">Пожаловаться</a>
                        </div>

                    </div>
                </div>
                <ul id="myTab" class="nav nav-tabs nav-top-border margin-top-20" role="tablist">
					<li role="presentation" class="active"><a href="#description" role="tab" data-toggle="tab">Описание</a></li>
					<li role="presentation"><a href="#maps" role="tab" data-toggle="tab">Карта</a></li>
				</ul>
                <div class="tab-content padding-top-20">
				
					<div role="tabpanel" class="tab-pane fade in active" id="description">
					   	<span itemprop="description" > <?=$product->description ?></span>
                          
                    </div>
					
				
					<div role="tabpanel" class="tab-pane fade" id="maps">
						   <?=YandexMap::widget([
                                'item' => $product['id'],
            
                            ]); ?>
					</div>

				</div>
                
             
            </div>



        </div>

    </div>
</section>


<?=Complaint::widget([
    'id_product' => $product['id'],

]); ?>




<?
$script = <<< JS
    $(function(){
        	jQuery("a.add-wishlist").click(function() {
			var item_id = jQuery(this).attr('data-item-id');
			if(parseInt(item_id) < 1) {
				return false;
			}

			$.ajax({
                url: '/shop/wishlist',
				data: 	{ajax:"true", action:'add_to_wishlist', item_id:item_id},
				type: 	"POST",
				error: 	function(XMLHttpRequest, textStatus, errorThrown) {
					_toastr("Ошибка. Пожалуйста повторите попытку позже.","top-right","error",false);
				},
				success: function(data) {
					data = data.trim(); // remove output spaces
					if(data == '_update_' || data == '_create_' ) {
						_toastr("Запчасть добавлена в закладки","top-right","success",false);
					}
					if(data == '_double_' ) {
						_toastr("Запчасть уже добавлена в закладки","top-right","warning",false);
					}
					if(data == '_invalid_id_' || data == '_error_' ) {
						_toastr("Ошибка. Пожалуйста повторите попытку позже.","top-right","error",false);

					}

				}
			});
            return false;
		});
		$('button.show-phone').click(function(){
            var item_id = jQuery(this).attr('data-item-id');
            $.ajax({
                url: '/parts/phone',
				data: 	{ajax:"true", item_id: item_id},
				type: 	"POST",
				error: 	function(XMLHttpRequest, textStatus, errorThrown) {
					_toastr("Ошибка. Пожалуйста повторите попытку позже.","top-right","error",false);
				},
				success: function(data) {
				console.log(data);
                var canvas=document.getElementById("canvas-phone");
                canvas.height = 40;
                var x = canvas.getContext("2d");
                x.font = "bold  15px Open Sans";
                x.fillStyle = '#797979';
                x.textAlign = "left";
                x.fillText('Телефон: '  + JSON.parse(data), 0, 20);
                $('#canvas-phone').show();
				}
			});

		});
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>