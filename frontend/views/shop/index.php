<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use frontend\widgets\filter\PartsFilter;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;


$this->title = $meta_title;
$this->registerMetaTag([
		'name' => 'description',
		'content' => $meta_description
]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $meta_title.', AllVinExpress, AllVin, AllVin Express, All, Vin, Express, All-Vin, покупка автозапчастей, автозапчасти, найти запчасть, продать автозапчасть, купить автозапчасть, продать, sale']);

$this->params['breadcrumbs'][] = $this->title;
?>
	<section class="padding-xs">
		<div class="container">

			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3 ">
					<div class="side-nav margin-bottom-60">
						<div class="side-nav-head">
							<button class="fa fa-bars"></button>
							<h4>Категории</h4>
						</div>
						<?=Menu::widget([
								'items' => $category,
								'options'=>['class'=>'list-group-item'],
								'submenuTemplate' => "\n	<ul>\n{items}\n</ul>\n",
								'linkTemplate' => '<a class="dropdown-toggle" href="{url}">{label}</a>',
								'itemOptions'=>array('class'=>'list-group-item'),
								'activateParents'=>true,
								'encodeLabels' => false,
								'options' => [
										'class' => 'list-group list-group-bordered list-group-noicon',
								],
						]);
						?>
					</div>

				</div>
				<div class="col-lg-9 col-md-9 col-sm-9">

					<h1 class="size-20"><?=$title;?></h1>

					<? if ($dataProvider->getCount()):?>

					<?=PartsFilter::widget(['full' => true]); ?>
						<div class="clearfix shop-list-options">
							<form>
								<div class="options-left">
									<strong>Сортировка:</strong>
									<select name="sort" onChange="this.form.submit()">
										<option value="newer">По новизне</option>
										<option value="name">По названию</option>
										<option value="price_desc">Дороже</option>
										<option value="price_asc">Дешевле</option>


									</select>
								</div>
							</form>
						</div>
						<div class="block-parts">
							<?$pjax = \yii\widgets\Pjax::begin();?>
							<?= ListView::widget([
									'dataProvider' => $dataProvider,
									'itemView' => '/shop/_view_small',
									'id' => 'my-listview-id',
									'layout' => "<ul itemscope itemtype='http://schema.org/ItemList' class=\"shop-item-list row list-inline nomargin\">{items}</ul><div class=\"shop-pegination\">{pager}</div>",

									'itemOptions' => [
											'tag' => 'li',
											'class' => 'item col-lg-3 col-md-3 col-sm-6 col-xs-12',
									],


									'pager' => [
											'class' => \kop\y2sp\ScrollPager::className(),
											'container' => '.shop-item-list',
											'item' => '.item',
											'paginationSelector' => '.pagination',

											'negativeMargin' => '200',
											'triggerText' => 'Загрузить',
											'triggerOffset' => 90000,
											'noneLeftText' => '',
											'triggerTemplate' => '',
											'enabledExtensions' => [
													\kop\y2sp\ScrollPager::EXTENSION_TRIGGER,
													\kop\y2sp\ScrollPager::EXTENSION_SPINNER,

													\kop\y2sp\ScrollPager::EXTENSION_PAGING,
											],
									],


							]);
							?>
							<?\yii\widgets\Pjax::end();?>

						</div>





					<? else:?>

						<div class="alert alert-warning text-center margin-bottom-30">
							<strong>Запчастей не найдено.</strong>
						</div>
					<? endif;?>
					<hr>
				</div>

			</div>
		</div>
	</section>




