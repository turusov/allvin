<?
    $img = "";
    if($model['mainImages']):
        $img = '/uploads/products/id'.$model->id_user.'/'.$model['mainImages']->image;
    else:
        $img =  '/template/images/noimg.png';
    endif;
?>
<div  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="shop-item">
    <meta itemprop="position" content="<?=$model->id?>" />
    <div class="thumbnail ">
        <?
        echo Yii::$app->easyImage->thumbOf($img,
            [
                'resize' => ['height'=>400],
            ],
            [
                'style' => [
                    'width' => '100%',
                    'height' => '200px'
                ],
                'class' => [' padding-0 blur'],
                'alt'=> $model->name,
                'title' => 'Автозапчаcть '.$model->name
            ]
        );
        ?>
         <a href="/parts/id<?=urlencode($model->id)?>">
               <?
               echo Yii::$app->easyImage->thumbOf($img,
               [
                   'resize' => ['height'=>200],
               ],
               [
                   'style' => [

                       'position' => 'absolute',
                       'padding' => 0,
                       'width' => '100%',
                       'height' => 'auto',
                       'margin' => 'auto',
                        'top' => 0,
                       'bottom' => 0,
                        'left' => 0,
                       'right' => 0,
                       'text-align' => 'center',
                       'display' => 'inline-block',
                       'max-width' => '400%'

                   ],
                   'class' => [],
                     'itemprop' =>'image',
                   'alt'=> $model->name,
                   'title' => 'Автозапчаcть '.$model->name
               ]
               );
               ?>
       </a>
    </div>

    <div class="shop-item-summary text-center">
        <a itemprop="url"  href="/parts/id<?=urlencode($model->id)?>">
            <h2 itemprop="name"><?=$model->name?></h2>
        </a>
        <div class="shop-item-price">
          
            <span>  <?=$model->price?> руб.</span>
        </div>
    </div>
</div>
