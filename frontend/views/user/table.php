<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title = $name.' AllVinExpess';
?>
<div class="heading-title heading-border ">
    <h1><?=$name?></h1>
</div>
<div class="table-responsive">
    <table class="table table-bordered table-striped ">
        <thead>
        <tr>
            <th>Наименование</th>
            <th>Номер</th>
            <th>Цена</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <?=ListView::widget(['dataProvider' => $provider, 'itemView' => '_tr',  'layout' => "{pager}\n{items}",]);?>
        </tbody>
    </table>
</div>
