<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Category;
use frontend\models\CarMark;
$this->title = 'Изменение';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="padding-xxs">
    <div class="container">
        <h1 class="size-20">Изменение автозапчасти</h1>
        <?php echo $this->render('_form_parts_update', [
            'product'=>$product,
            'category_product' => $category_product,
            'productMark' => $productMark,
            'productModel' => $productModel,
            'userAddressModel' => $userAddressModel,
            'partsImageModel' => $partsImageModel
        ]); ?>
    </div>
</section>

