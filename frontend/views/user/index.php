<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title = 'Профиль';
?>

<section class="page-header light page-header-xs">
    <div class="container">
        <h1 class="size-30">
            <?=$user->username?></span>
        </h1>

    </div>
</section>

<section>
    <div class="container">
        <h3 class="size-20">Ваши последнии добавления</h3>
        <? if ($product->getCount()):?>
        <ul class="shop-item-list row list-inline nomargin ">
            <?= ListView::widget([
                'dataProvider' => $product,
                'itemView' => '_product',
                'layout' => "{items}",
            ]);
            ?>
        </ul>
        <? else:?>

            <div class="alert alert-warning text-center margin-bottom-30">
                <strong>Записей не найдено.</strong>
            </div>
        <? endif;?>
    </div>
</section>
<section>
    <div class="container">
        <h3 class="size-20">Ваши закладки</h3>
        <? if ($favorite->getCount()):?>
        <ul class="shop-item-list row list-inline nomargin ">
            <?= ListView::widget([
                'dataProvider' => $favorite,
                'itemView' => '_review',
                'layout' => "{items}",
            ]);
            ?>
        </ul>
        <? else:?>

            <div class="alert alert-warning text-center margin-bottom-30">
                <strong>Записей не найдено.</strong>
            </div>
        <? endif;?>
    </div>
</section>


<section>
    <div class="container">
        <h3 class="size-20">Вы недавно смотрели</h3>
        <? if ($reviews->getCount()):?>
        <ul class="shop-item-list row list-inline nomargin ">
            <?= ListView::widget([
                'dataProvider' => $reviews,
                'itemView' => '_review',
                'layout' => "{items}",
            ]);
            ?>
        </ul>
        <? else:?>

            <div class="alert alert-warning text-center margin-bottom-30">
                <strong>Записей не найдено.</strong>
            </div>
        <? endif;?>
    </div>
</section>
