<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title = $name;
?>
<section>
    <div class="container">
        <h1 class="size-20">Ваши закладки</h1>
        <? if ($provider->getCount()):?>
        <ul class="shop-item-list row list-inline nomargin padding-top-20">
            <?= ListView::widget([
                'dataProvider' => $provider,
                'itemView' => '_review',
                'layout' => "{items}",
                'viewParams' => ['remove' => 1],
            ]);
            ?>
        </ul>
        <? else:?>

            <div class="alert alert-warning text-center margin-bottom-30">
                <strong>Записей не найдено.</strong>
            </div>
        <? endif;?>
    </div>
</section>

<?php
/**
 * Связанные списки
 */
$script = <<< JS
    $('.remove-wishlist').click(function(){
        var id = $(this).attr('data-item-id');
        $.post( "", { id: id});
        $(this).parents('.shop-item').parent().remove();
        _toastr("Запчасть была удалена","top-right","error",false);
        return false;
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
