<?
use yii\helpers\Html;
use frontend\models\PartsImage;
?>
<li class="col-lg-3 col-sm-3 col-xs-6">
       <div class="shop-item">
        <div class="thumbnail">
            <? $image = PartsImage::getImageById($model['products']->id)?>
            <? if($image) { $image = "/uploads/products/id".$model['products']->id_user."/".$image;  } else { $image= '/template/images/noimg.png'; } ?>

        <?
        echo Yii::$app->easyImage->thumbOf($image,
            [
                'resize' => ['height'=>400],
            ],
            [
                'style' => [
                    'width' => '100%',
                    'height' => '200px'
                ],
                'class' => [' padding-0 blur'],
                'alt'=> $model['products']->name,
                'title' => 'Автозапчаcть '.$model['products']->name
            ]
        );
        ?>
        <a href="/parts/id<?=urlencode($model['products']->id)?>" class="">

            <?
            echo Yii::$app->easyImage->thumbOf($image,
                [
                    'resize' => ['height'=>200],

                ],
                [
                    'style' => [

                        'position' => 'absolute',

                        'padding' => 0,
                        'width' => '100%',
                        'height' => 'auto',
                        'margin' => 'auto',
                        'top' => 0,
                        'bottom' => 0,
                        'left' => 0,
                        'right' => 0,
                        'text-align' => 'center',
                        'display' => 'inline-block',
                        'max-width' => '400%'

                    ],
                    'class' => [],
                    'alt'=> $model['products']->name,
                    'title' => 'Автозапчаcть '.$model['products']->name
                ]
            );
            ?>







            <? if($remove):?>
            <div class="shop-option-over">
                <a class="btn btn-default remove-wishlist" href="#" data-item-id="<?=$model['products']->id?>" data-toggle="tooltip" title="" data-original-title="Удалить из избранных"><i class="glyphicon glyphicon-remove nopadding"></i></a>
            </div>
            <? endif;?>
        </div>
        <div class="shop-item-summary text-center">
            <a href="/parts/id<?=urlencode($model['products']->id)?>">
                <h2><?=$model['products']->name?></h2>
            </a>
            <div class="shop-item-price">
                <?=$model['products']->price?> руб.
            </div>
        </div>
    </div>
</li>

