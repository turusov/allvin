<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
$this->title = 'Добавление адреса';

?>
<section>
    <div class="container">
        <div class="pull-right">
            <a href="/user/address" class="btn btn-warning">Назад</a>
        </div>
        <h1 class="size-20">Добавление адреса</h1>
        <?php echo $this->render('_form_address', [
            'userAddress' => $userAddress
        ]); ?>

    </div>
</section>
<?php
/**
 * Связанные списки
 */
$script = <<< JS
jQuery('#useraddress-id_region').change(function(){
    $('#useraddress-id_city option').remove();
    $.ajax({
        url: '/city',
        type: "POST",
        data: {
            'id_region': $(this).val(),
        },
        success: function (data) {
            $('#useraddress-id_city ').append(data);
        }
    });
})
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
