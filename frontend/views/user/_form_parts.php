<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Category;
use frontend\models\CarMark;
use frontend\models\CarModel;
use frontend\models\UserAddress;
use frontend\models\Regions;

?>
<ul class="nav nav-tabs nav-justified">
	<li class="active"><a href="#info" data-toggle="tab">Информация</a></li>
    <li><a href="#photo" data-toggle="tab">Фото</a></li>
    <li><a href="#contacts" data-toggle="tab">Контакты</a></li>
</ul>



<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,

]); ?>

<div class="tab-content">
	<div class="tab-pane fade in active" id="info">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <?= $form->field($product, 'name')->textInput() ?>
            </div>
            <div class="col-md-6 col-sm-12">
                <?= $form->field($product, 'number')->textInput() ?>
            </div>

        </div>
        <div class="row">

            <div class="col-md-4 col-sm-6">
                <label class="control-label" for="productmark-id_mark">Марка</label>
                <div class="fancy-form fancy-form-select">
                    <?=$form->field($productMark, 'id_mark', ['inputOptions' =>['class'=> 'form-control required  select2']])->dropdownList(
                        CarMark::find()->select(['name', 'id_car_mark'])->indexBy('id_car_mark')->column(), array('prompt'=>'Марка автомобиля'))->label(false);
                    ?>
                    <i class="fancy-arrow-double"></i>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">

                <?if(!$productModel->id_model):?>
                    <?=$form->field($productModel, 'id_model', ['template'=>'
                        {label}
                        <div class="fancy-form fancy-form-select">
                           {input}
                            <i class="fancy-arrow-double"></i>
                        </div>
                        {error}
                        '],
                        ['inputOptions'=>['class'=> 'form-control required  select2']])->dropdownList([
                        'prompt' => 'Выберите марку автомобиля...'])->label();
                    ?>
                <? else:?>

                    <?=$form->field($productModel, 'id_model', ['template'=>'
                        {label}
                        <div class="fancy-form fancy-form-select">
                           {input}
                            <i class="fancy-arrow-double"></i>
                        </div>
                        {error}
                        '],
                        ['inputOptions'=>['class'=> 'form-control required  select2']])->dropdownList(
                        CarModel::find()->select(['name', 'id'])->where(['id_car_mark' =>$productMark->id_mark])->indexBy('id')->column(), array('prompt'=>'Марка автомобиля')

                    )->label();
                    ?>



                <?endif;?>
            </div>
            <div class="col-md-4 col-sm-12">

                <label class="control-label" for="productmark-id_mark">Категория</label>

                    <?=$form->field($category_product, 'id', ['inputOptions' =>['class'=> 'form-control select2 required ']])->dropdownList(array_merge(
                        ['prompt' => 'Выберите категорию запчасти'], Category::find()->select(['name', 'id'])->indexBy('id')->column()))->label(false);
                    ?>

            </div>
            <div class="col-md-12 col-sm-12">
                <?= $form->field($product, 'price', ['template'=>'{label}<div class="input-group">{input}<span class="input-group-addon">.руб</span></div>{error}'])->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <?= $form->field($product, 'description', ['template'=>'
                 {label}
                
                    <div class="fancy-form margin-top-20">
                        {input}


                    </div>
                    
                
                {error}
                '])->textarea(['rows' => '6', 'style'=>'padding-left:10px;', 'class' => 'form-control markdown', 'data-lang'=>'ru', 'data-info'=>'textarea-words-info']);?>



            </div>
        </div>
	</div>
    <div class="tab-pane fade" id="photo">
        <div class="row">

            <div class="col-md-12">
                <div class="row images">
                </div>

                <? echo \kato\DropZone::widget([
                    'options' => [
                        'maxFilesize' => '2',
                        'class' => 'dropzone',
                        'url' => \Yii::$app->getUrlManager()->createUrl(['api/uparts'])
                    ],
                    'clientEvents' => [
                        'removedfile' => "function(file){alert(file.name + ' is removed')}",
                        'success' => "
                    function(file){
                        console.log(file.xhr.response);
                        $('#photo .images').append(file.xhr.response);
                    }",
                        'error' => "
                    function(file){
                        //console.log(file.xhr.response);
                    }"
                    ],
                ]);
                ?>

            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="contacts">
        <?=$form->field($product, 'id_address')->dropdownList(array_merge(
            ['prompt' => 'Адрес'],
            UserAddress::find()->select(['address', 'id'])->where(['id_user' => \Yii::$app->user->identity->id, 'active' => 1])->indexBy('id')->column()))->label('Адрес');
        ?>
        <a class="#" data-toggle="modal" data-target="#add-address">Добавить адрес</a>
    </div>
</div>
  <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end() ?>
<div id="add-address" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Добавление нового адреса</h4>
            </div>

            <?php $form = ActiveForm::begin([
                'action' => ['create'],
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,

            ]); ?>
            <div class="modal-body">
                <?=$form->field($userAddressModel, 'id_region')->dropdownList(
                    Regions::find()->select(['title_ru', 'id'])->where(['country_id' => \Yii::$app->params['defaultRegion']])->orderBy('title_ru')->indexBy('id')->column(), array('prompt'=>'Регион'));
                ?>
                <?=$form->field($userAddressModel, 'id_city')->dropdownList([
                    'prompt' => 'Выберите город...']);
                ?>

                <?= $form->field($userAddressModel, 'address')->textInput() ?>
            </div>
            <div class="modal-footer">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<?php
/**
 * Связанные списки
 */
$script = <<< JS

$('.images').on('click', ".remove-img", function(){
    $(this).parents('.item-img').remove();
});

$('#add-address').submit(function(){
    var form = $(this);
    var address = $(this).find('form').serializeArray();
    $.ajax({
        url: '/api/address',
        type: "POST",
        data: address,
        success: function (data) {
            $('#contacts').find('select').append(data);
            $(form).modal(false);
            $('.modal-backdrop').remove();
     
             _toastr("Адрес добавлен","top-right","info",false);
        }
    });
    return false;
});
$('#productmark-id_mark.form-control').change(function(){
    $('#productmodel-id_model option').remove();
    $.ajax({
        url: '/models',
        type: "POST",
        data: {
            'marks':$(this).val(),
        },
        success: function (data) {
            $('#productmodel-id_model').append(data);
        }
    });
})
jQuery('#useraddress-id_region').change(function(){
    $('#useraddress-id_city option').remove();
    $.ajax({
        url: '/city',
        type: "POST",
        data: {
            'id_region': $(this).val(),
        },
        success: function (data) {
            $('#useraddress-id_city ').append(data);
        }
    });
})
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>



