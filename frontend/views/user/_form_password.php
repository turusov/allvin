<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use frontend\models\Regions;
use frontend\models\Cities;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,

]); ?>
<?= $form->field($user, 'old_password')->passwordInput()->label('Старый пароль') ?>
<?= $form->field($user, 'password')->passwordInput(['value'=>''])->label('Новый пароль') ?>

<?=Html::submitButton('Изменить', ['class' => 'btn btn-info']) ?>
<?php ActiveForm::end() ?>