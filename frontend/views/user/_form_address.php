<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use frontend\models\Regions;
use frontend\models\Cities;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,


]); ?>



<?=$form->field($userAddress, 'id_region', ['inputOptions'=>['class'=> 'form-control select2']])->dropdownList(
    Regions::find()->select(['title_ru', 'id'])->where(['country_id' => \Yii::$app->params['defaultRegion']])->orderBy('title_ru')->indexBy('id')->column(), array('prompt'=>'Регион'));
?>

<?if ($userAddress->id_city):?>
    <?=$form->field($userAddress, 'id_city', ['inputOptions'=>['class'=> 'form-control select2']])->dropdownList(
        Cities::find()->select(['title_ru', 'id'])->where(['region_id'=> $userAddress->id_region])->orderBy('title_ru')->indexBy('id')->column(), array('prompt'=>'Регион'));
    ?>
<?else:?>
    <?=$form->field($userAddress, 'id_city', ['inputOptions'=>['class'=> 'form-control select2']])->dropdownList([
        'prompt' => 'Выберите город...']);
    ?>
<?endif;?>
<?= $form->field($userAddress, 'address')->textInput() ?>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-info']) ?>
<?php ActiveForm::end() ?>


