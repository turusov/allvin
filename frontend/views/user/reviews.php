<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title = $name.' История просмотров';
?>
<section>
    <div class="container">
        <h1 class="size-20">Вы недавно смотрели</h1>
        <? if ($provider->getCount()):?>
            <ul class="shop-item-list row list-inline nomargin padding-top-20">
                <?= ListView::widget([
                    'dataProvider' => $provider,
                    'itemView' => '_review',
                    'layout' => "{items}",
                ]);
                ?>
            </ul>
        <? else:?>
            <div class="alert alert-warning text-center margin-bottom-30">
                <strong>Записей не найдено.</strong>
            </div>
        <? endif;?>
    </div>
</section>