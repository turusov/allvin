<tr>
    <td><?=$model['regions']->title_ru?></td>
    <td><?=$model['city']->title_ru?></td>
    <td><?=$model->address?></td>

    <td class="text-right">
        <?if($model->main):?>
            <span class="label label-success">Основной </span>
        <?else:?>
            <a href="#" data-item-id="<?=$model->id?>" class="btn btn-default btn-xs address-main"><i class="fa fa-check white"></i> Сделать основным </a>
        <?endif;?>
        <a href="/user/address/edit/id<?=$model->id?>" class="btn btn-default btn-xs"><i class="fa fa-edit white"></i> Изменить </a>
        <a href="#" data-item-id="<?=$model->id?>" class="btn btn-default btn-xs address-remove"><i class="fa fa-times white"></i> Удалить </a>
    </td>
</tr>

