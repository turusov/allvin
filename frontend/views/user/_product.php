<?
use yii\helpers\Html;
use frontend\models\PartsImage;


?>
<?
$img = "";
if($model['mainImages']):
    $img = '/uploads/products/id'.$model->id_user.'/'.$model['mainImages']->image;
else:
    $img =  '/template/images/noimg.png';
endif;
?>
<li class="col-lg-3 col-sm-3 col-xs-6">
    <div class="shop-item">
        <div class="thumbnail ">

            <?
            echo Yii::$app->easyImage->thumbOf($img,
                [
                    'resize' => ['height'=>400],
                ],
                [
                    'style' => [
                        'width' => '100%',
                        'height' => '200px'
                    ],
                    'class' => [' padding-0 blur'],
                    'alt'=> $model->name,
                    'title' => 'Автозапчаcть '.$model->name
                ]
            );
            ?>
            <a href="/parts/id<?=urlencode($model->id)?>">
                <?
                echo Yii::$app->easyImage->thumbOf($img,
                    [
                        'resize' => ['height'=>200],

                    ],
                    [
                        'style' => [

                            'position' => 'absolute',

                            'padding' => 0,
                            'width' => '100%',
                            'height' => 'auto',
                            'margin' => 'auto',
                            'top' => 0,
                            'bottom' => 0,
                            'left' => 0,
                            'right' => 0,
                            'text-align' => 'center',
                            'display' => 'inline-block',
                            'max-width' => '400%'

                        ],
                        'class' => [],
                        'alt'=> $model->name,
                        'title' => 'Автозапчаcть '.$model->name
                    ]
                );
                ?>
            </a>
            <? if (isset($model->active) && $_SERVER['REQUEST_URI'] != "/user"):?>
            <div class="shop-option-over">
                <a class="btn btn-default product-edit" href="/user/parths/edit/id<?=$model->id?>" data-item-id="2" data-toggle="tooltip" title="" data-original-title="Изменить"><i class="glyphicon glyphicon-pencil nopadding"></i></a>

                <? if ($model->active):?>
                    <a class="btn btn-default product-hide" href="javascript:void(0)" data-item-id="<?=$model->id?>" data-toggle="tooltip" title="" data-original-title="Скрыть от всех"><i class="glyphicon glyphicon-eye-close nopadding"></i></a>
                <? else:?>
                    <a class="btn btn-default product-show" href=javascript:void(0)" data-item-id="<?=$model->id?>" data-toggle="tooltip" title="" data-original-title="Показать всем"><i class="glyphicon glyphicon-eye-open nopadding"></i></a>
                <? endif;?>
                <a class="btn btn-default product-remove" href="javascript:void(0)" data-item-id="<?=$model->id?>" data-toggle="tooltip" title="" data-original-title="Удалить"><i class="glyphicon glyphicon-remove nopadding"></i></a>

            </div>
            <? endif;?>
        </div>


        <div class="shop-item-summary text-center">
            <a href="/parts/id<?=urlencode($model->id)?>">
                <h2><?=$model->name?></h2>
            </a>
            <div class="shop-item-price">
                <?=$model->price?> руб.
            </div>
        </div>
    </div>
</li>

