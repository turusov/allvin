<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title ='Ваши запчасти';
?>
<section>
    <div class="container">
        <div class="pull-right">
            <a href="/user/parths/add" class="btn btn-primary">Добавить запчасть</a>
        </div>
        <h1 class="size-20">Ваши запчасти</h1>

        <? if ($provider->getCount()):?>
            <ul class="shop-item-list row list-inline nomargin padding-top-20 ">
                <?= ListView::widget([
                    'dataProvider' => $provider,
                    'itemView' => '_product',
                    'layout' => "{items}",
                    'viewParams' => [
                        'edit' => 1,
                        'remove' => 1,
                        'active' => 1,
                    ],
                ]);
                ?>
            </ul>
        <? else:?>
            <div class="alert alert-warning text-center margin-bottom-30">
                <strong>Записей не найдено.</strong>
            </div>
        <? endif;?>
    </div>
</section>

<?php
/**
 * Связанные списки
 */
$script = <<< JS
    $('.shop-item .product-hide').click(function(){
        var id = $(this).attr('data-item-id');
        if (confirm("Вы уверены что хотите скрыть запчасть?")) {
        $.post( "", { id: id, action: 'hide'});
        _toastr("Запчасть была скрыта","top-right","info",false);
        return false;
        }
    });
     $('.shop-item .product-show').click(function(){
        var id = $(this).attr('data-item-id');
        if (confirm("Вы уверены что хотите показать запчасть?")) {
        $.post( "", { id: id, action: 'show'});
        _toastr("Запчасть стала доступна для всех","top-right","success",false);
        }
        return false;
    });
     $('.shop-item .product-remove').click(function(){
        var id = $(this).attr('data-item-id');
        if (confirm("Вы уверены что хотите удалить?")) {
            $.post( "", { id: id, action: 'remove'});
             $(this).parents('.shop-item').parent().remove();
            _toastr("Запчасть была удалена","top-right","warning",false);
        }
        return false;
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
