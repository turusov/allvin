<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title ='Ваши запчасти';
?>
<section>
    <div class="container">
        <div class="pull-right">
            <a href="/user/address/create" class="btn btn-primary">Добавить адреса</a>
        </div>
        <h1 class="size-20">Ваши адреса</h1>
        <? if ($address->getCount()):?>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Регион</th>
                    <th>Город</th>
                    <th>Адрес</th>

                    <th class="text-right">Действие</th>
                </tr>
                </thead>
                <tbody>
                    <?=ListView::widget(['dataProvider' => $address, 'itemView' => '_address',  'layout' => "{pager}\n{items}",]);?>
                </tbody>
            </table>
        </div>
        <? else:?>

            <div class="alert alert-warning text-center margin-bottom-30">
                <strong>Записей не найдено.</strong>
            </div>
        <? endif;?>

    </div>
</section>
<?php
$script = <<< JS
    $('.table-responsive .address-main').bind('click',function(){
        var id = $(this).attr('data-item-id');
        $.post( "", { id: id, action: 'main'});
        var old = $(this).parents('tbody').find('.label.label-success');
        $(old).parent().find('.address-remove').attr('data-item-id');
        $(old).parent().prepend(' <a href="#" data-item-id='+$(old).parent().find('.address-remove').attr('data-item-id')+' class="btn btn-default btn-xs address-main"><i class="fa fa-check white"></i> Сделать основным </a>');
        $(old).remove();


        $(this).parent().prepend('<span class="label label-success">Основной </span>');
        $(this).remove();
        _toastr("Изменено","top-right","info",false);
        return false;
    });
      $('.table-responsive .address-remove').bind('click',function(){
        var id = $(this).attr('data-item-id');
        if (confirm("Вы уверены что хотите удалить?")) {
            if(!$(this).parent().find('.label.label-success').html()) {
                $.post( "", { id: id, action: 'remove'});
                $(this).parents('tr').remove();
                _toastr("Адрес удален","top-right","warning",false);
            } else {
                _toastr("Нельзя удалить основной адрес","top-right","warning",false);
            }
        }

        return false;
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
