<?php
use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
$this->title = 'Изменение пароля';

?>
<section>
    <div class="container">
        <div class="pull-right">
            <a href="#" onclick="history.back();" class="btn btn-warning">Назад</a>
        </div>
        <h1 class="size-20">Изменение пароля</h1>
        <?php echo $this->render('_form_password', [
            'user' => $userModel
        ]); ?>
    </div>
</section>