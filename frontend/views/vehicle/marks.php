<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Марки';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'AllVinExpress, Поиск, по маркам автомобилей, AllVin, AllVin Express, All, Vin, Express, All-Vin, покупка автозапчастей, автозапчасти, найти запчасть, продать автозапчасть, купить автозапчасть, продать, sale']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Поиск автозапчастей по маркам автомобилей']);
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="padding-xs">
	<div class="container">


		<h1 class="size-20">Марки</h1>
		<ul class="row clients-dotted padding-top-20 list-inline">
            <?foreach($marks as $mark):?>
            <li class="col-md-3 col-sm-3 col-sm-4 col-xs-6">
				<a href="/marks/<?=urlencode($mark['name'])?>">
					<?
						echo Yii::$app->easyImage->thumbOf('/uploads/marks/'.mb_strtolower($mark['logo'], 'UTF-8'),
								[
										'resize' => ['height' => 80],

								],
								[
										'class' => 'img-responsive',
										'style' => 'height: 80px',
										'alt'=> $mark['name'],
										'title' => 'Автомобильная марка '.$mark['name']
								]

						);
					?>

                </a>
				<h4 class="padding-top-10"><a href="/marks/<?=urlencode($mark['name'])?>"><?=$mark['name']?></a></h4>

			</li>
             <?endforeach;?>
 		</ul>
	</div>
</section>