<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Автозапчасти для автомобиля '.$mark['name'].' '.$name;
$this->registerMetaTag(['name' => 'keywords', 'content' => 'AllVinExpress, Автозапчасти для автомобилей марки '.$name.', покупка и продажа автозапчастей, автозапчасти, найти запчасть, продать автозапчасть, купить автозапчасть, запчасти для '.$name.', бу запчасти для '.$name.'оригинальные запчасти для '.$name.','.$name]);
$this->registerMetaTag(['name' => 'description', 'content' => 'Автозапчасти для автомобилей марки '.$name]);
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="padding-xs">
    <div class="container">
    <h1  class="size-20"><?=$name?></h1>

		<ul class="row clients-dotted padding-top-20 list-inline">
			<?foreach($models as $model):?>
				<li class="col-md-3 col-sm-3 col-sm-4 col-xs-6">
					<a href="/marks/<?=urlencode($mark['name'])?>">
						<h4 class="padding-top-10" style="color: #797979;"><a href="/marks/<?=$name?>/<?=$model->name?>"><?=$model->name?></a></h4>
					</a>
				</li>
			<?endforeach;?>
		</ul>
</section>