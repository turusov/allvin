<?
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;

$this->title = 'Бренды автозапчастей AllVinExpess';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'автозапчасти, бренды, производители, allvinexpress', 'оригинальные автозапчасти', 'новые автозапчасти', 'б.у. автозапчасти']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Бренды автозапчастей']);
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="padding-xs">
    <div class="container">
        <h1 class="size-20">Бренды</h1>


            <? Pjax::begin(); ?>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                'layout' => "{items} {pager}",
                'options' => [
                    'tag' => 'ul',
                    'class' => 'row clients-dotted padding-top-20 list-inline',

                ],
                'itemOptions' => [
                    'tag' => 'li',
                    'class' => 'item col-md-2 col-sm-3 col-sm-4 col-xs-6',
                ],
                'pager' => [
                    'class' => \kop\y2sp\ScrollPager::className(),
                    'container' => '.clients-dotted',
                    'item' => '.item',
                   'negativeMargin' => '200',
                    'triggerText' => 'Загрузить',
                    'triggerOffset' => 90000,
                    'noneLeftText' => '',
                    'triggerTemplate' => '<div class="ias-trigger" style="text-align: center; cursor: pointer;"><a>{text}</a></div>',
                    'enabledExtensions' => [
                        \kop\y2sp\ScrollPager::EXTENSION_TRIGGER,
                        \kop\y2sp\ScrollPager::EXTENSION_SPINNER,
                        \kop\y2sp\ScrollPager::EXTENSION_NONE_LEFT,
                        \kop\y2sp\ScrollPager::EXTENSION_PAGING,
                    ],
                ],
            ]);
            ?>
            <?Pjax::end();?>

    </div>
</section>
