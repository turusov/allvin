<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use frontend\models\Category;
$this->title = 'Добавление категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<section>
    <div class="container">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Основная информация</strong>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                    <br>
                    <?= $form->field($model, 'url')->textInput(['autofocus' => true]) ?>
                    <br>

                    <?=$form->field($model, 'parent_id') ->dropDownList(ArrayHelper::map(Category::find()->where('id <> :id', [':id' => $model->id])->all(), 'id', 'name'),  ['prompt'=>'Категории', 'options' => array($model->parent_id=>array('selected'=>true))])
                    ?>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>SEO</strong>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'meta_key')->textInput(['autofocus' => true]) ?>
                    <br>
                    <div class="alert alert-mini alert-warning margin-bottom-30"><!-- WARNING -->
                        <strong>Внимание!</strong> Ключевые слова указываются через запятую или пробел и могут быть написаны в любом регистре. Рекомендуется указывать не более 10-15 ключевых слов или словосочетаний.
                    </div>
                      <?= $form->field($model, 'meta_description')->textInput(['autofocus' => true]) ?>
                </div>
            </div>
        </div>
        <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
        </div>
          <?php ActiveForm::end(); ?>
    </div>
</section>

