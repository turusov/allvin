<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => 'AllVinExpress, AllVin, AllVin Express, авторизация, All, Vin, Express, All-Vin, покупка автозапчастей, автозапчасти, найти запчасть, продать автозапчасть, купить автозапчасть, продать, sale']);
$this->registerMetaTag(['name' => 'description', 'content' => 'AllVinExpress авторизация пользователя']);


?>
<section class="padding-xs">
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
 <h1>Авторизация</h1>
  <?php $form = ActiveForm::begin(['id' => 'login-form', 'class'=>'sky-form']); ?>
                                <div class="clearfix">
                                    <div class="form-group">
                                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model, 'password')->passwordInput() ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <!-- Inform Tip -->
                                        <div class="form-tip pt-20">
                                           <?= Html::a('Забыли пароль?', ['site/request-password-reset'], ['class'=>'no-text-decoration size-13 margin-top-10 block bold']) ?>.
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                    </div>

                                </div>

                            <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</section>