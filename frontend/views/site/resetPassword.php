<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>

<section  class="padding-xs">
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <h1>Восстановление пароля</h1>
                        <p> Пожалуйста, введите новый пароль :</p>
                      
                            <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'class'=>'sky-form']); ?>
                            <div class="clearfix">
                                <div class="form-group">
                                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                                </div>

                            </div>

                            <?php ActiveForm::end(); ?>


            </div>
        </div>

    </div>
</section>