<?php
$this->title = 'AllVinExpress - место где покупают и продают запчасти';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'AllVinExpress, AllVin, AllVin Express, All, Vin, All-Vin, покупка автозапчастей, автозапчасти, найти запчасть, автозапчасть, купить автозапчасть, найти запчасть,
купить автозапчасть,
купить бу запчасть,
купить бу автозапчасть,
найти запчасть по каталогу ,
поиск запчасти по каталогу ']);
$this->registerMetaTag(['name' => 'description', 'content' => 'AllVinExpress развивающаяся интернет портал по продаже или покупки автозапчастей. Здесь вы можете выгодно и быстро продать или купить автозопчасть.']);

$this->registerMetaTag(['property' => 'og:title', 'content' => 'AllVinExpress - место где покупают и продают запчасти']);
$this->registerMetaTag(['property' => 'og:description', 'content' => 'AllVinExpress развивающаяся интернет портал по продаже или покупки автозапчастей. Здесь вы можете выгодно и быстро продать или купить автозопчасть.']);
$this->registerMetaTag(['property' => 'og:image', 'content' => '/template/images/logo.png']);



$this->registerCssFile('template/plugins/slider.layerslider/css/layerslider.css');
use yii\widgets\ListView;
use yii\widgets\Menu;
use frontend\widgets\filter\PartsFilter;
use frontend\widgets\yandexMap\YandexMap;
?>
<h1 class="hidden-lg hidden-md hidden-sm hidden-xs">AllVinExpress</h1>
<section class="padding-0">
            <div id="slider"   class="slider fullwidthbanner-container roundedcorners">
                <div class="fullscreenbanner"  data-navigationStyle="">
                    <ul class="hide">
                        <li data-transition="slideleft"  data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Найти автозапчасть">
                            <img data-lazyload="/uploads/slide/slide3.jpg" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" />
                            <div class="tp-caption customin ltl large_bold_white"
                                 data-x="center"
                                 data-y="250"
                                 data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                 data-speed="800"
                                 data-start="1200"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-endeasing="Power4.easeIn" style="z-index: 10;">
                                Найди свою автозапчасть
                            </div>
                            <div class="tp-caption customin ltl  large_bold_white "
                                 data-x="center"
                                 data-y="338"
                                 data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                 data-speed="800"
                                 data-start="1550"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-endeasing="Power4.easeIn" style="z-index: 10;">
                                <a href="/parts" class="btn btn-primary btn-lg">
                                    <span><h2 class="margin-bottom-0 size-20">Найти</h2></span>
                                </a>
                            </div>
                        </li>

                        <li data-transition="slideleft"  data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Регистрация">
                            <img data-lazyload="/uploads/slide/slide2.jpg" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" />
                            <div class="tp-caption customin ltl large_bold_white"
                                 data-x="center"
                                 data-y="230"
                                 data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                 data-speed="800"
                                 data-start="1200"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-endeasing="Power4.easeIn" style="z-index: 10;">
                                Добавь свою автозапчасть
                            </div>
                            <div class="tp-caption customin ltl large_bold_white"
                                 data-x="center"
                                 data-y="338"
                                 data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                 data-speed="800"
                                 data-start="1550"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-endeasing="Power4.easeIn" style="z-index: 10;">
                                <? if(!Yii::$app->user->identity):?>
                                    <a href="/registration" class="btn btn-primary btn-lg">
                                        <span><h2 class="margin-bottom-0 size-20">Регистрация</h2></span>
                                    </a>
                                <?else:?>
                                    <a href="/shop/add" class="btn btn-primary btn-lg">
                                        <span><h2 class="margin-bottom-0 size-20">Добавление автозапчасти</h2></span>
                                    </a>
                                <?endif;?>
                            </div>

                        </li>
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>




</section>

<?
$this->registerJsFile('/template/plugins/slider.revolution/js/jquery.themepunch.tools.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/template/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/template/js/view/demo.revolution_slider.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<link href="/template/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
<link href="/template/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />


<section>
    <div class="container">
        <h3 class="size-20">Бренды</h3>
        <ul class="row clients-dotted padding-top-20 list-inline">
            <?foreach($marks as $mark):?>
                <li class="col-md-2 col-sm-3 col-sm-4 col-xs-6">
                    <a href="/marks/<?=urlencode($mark['name'])?>">
                        <?
                        echo Yii::$app->easyImage->thumbOf('/uploads/marks/'.mb_strtolower($mark['logo'], 'UTF-8'),
                            [
                                'resize' => ['height' => 80],
                                'quality' => 70,
                                 // 'background' => '#ffffff',
                                 'type' => 'png',
                            ],
                            [
                                'class' => 'img-responsive',
                                'style' => 'height: 80px',
                                'alt'=> $mark['name'],
                                'title' => 'Автомобильная марка '.$mark['name']
                            ]

                        );
                        ?>
                    </a>
                    <h4 class="padding-top-10"><a href="/marks/<?=urlencode($mark['name'])?>"><?=$mark['name']?></a></h4>

                </li>
            <?endforeach;?>
        </ul>
    </div>
</section>

<section>
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <h3 class="size-20">Новинки каталога </h3>
            	<? if ($products->getCount()):?>
                <?= ListView::widget([
                    'dataProvider' => $products,
                    'itemView' => '/shop/_view_small',
                    'layout' => '<ul class="shop-item-list padding-top-20 row list-inline nomargin" itemtype="http://schema.org/ItemList" itemscope>{items}</ul>',
                    
                    'itemOptions' => [
                        'tag' => 'li',
                        'class' => 'col-lg-3 col-md-3 col-sm-6 col-xs-12',
                    ],
                ]);
                ?>
            	<? else:?>

					<div class="alert alert-warning text-center margin-bottom-30">
						<strong>Запчастей не найдено.</strong>
					</div>
				<? endif;?>

        </div>
    </div>
</section>
<section>
    <div class="container">
        <h3 class="size-20">Карта автозапчастей</h3>
    </div>
    <?=YandexMap::widget(); ?>

</section>