<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля AllVinExpress';
$this->params['breadcrumbs'][] = $this->title;
?>

<section  class="padding-xs">
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <h1>Восстановление пароля</h1>
                       
                            <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'class'=>'sky-form']); ?>
                            <div class="clearfix">
                                <div class="form-group">
                                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-xs-6">

                                    <?= Html::submitButton('Восстановить', ['class' => 'btn btn-primary']) ?>
                                </div>

                            </div>

                            <?php ActiveForm::end(); ?>


            </div>
        </div>

    </div>
</section>