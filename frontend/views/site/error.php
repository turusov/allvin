<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<section class="padding-xlg">
    <div class="container">

        <h1><?= Html::encode($this->title) ?></h1>
        <p class="nomargin-top size-20 font-lato text-muted">  <?= nl2br(Html::encode($message)) ?></p>
        <a class="size-20 font-lato" href="/"><i class="glyphicon glyphicon-menu-left margin-right-10 size-16"></i> Назад</a>

    </div>
</section>
