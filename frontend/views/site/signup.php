<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => 'AllVinExpress, AllVin, AllVin Express, регистрация, All, Vin, Express, All-Vin, покупка автозапчастей, автозапчасти, найти запчасть, продать автозапчасть, купить автозапчасть, продать, sale']);
$this->registerMetaTag(['name' => 'description', 'content' => 'AllVinExpress регистрация нового пользователя']);
?>

<section class="padding-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h1>Регистрация</h1>
                <?php $form = ActiveForm::begin(['id' => 'form-signup', 'class'=>'sky-form']); ?>
                <div class="clearfix">
                    <div class="form-group">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'email') ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>