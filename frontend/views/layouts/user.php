<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div id="page-menu" class="page-menu-light">
    <div class="container">

        <nav class="pull-right"><!-- page menu -->
            <button id="page-menu-mobile" class="fa fa-bars"></button>
            <!--
                .menu-scrollTo - for scrollto page menu / no external links
            -->
            <ul class="list-inline">
                <li class="active"><a href="/user">Главная</a></li>
                <li><a href="/user/parths">Запчасти</a></li>
                <li><a href="/user/message">Сообщения</a></li>
                <li><a href="/user/favorites">Закладки</a></li>

                <li><a href="/user/reviews">История просмотров</a></li>

                <li><a href="/user/setting">Настройки</a></li>
            </ul>
        </nav><!-- /page menu -->

    </div>
</div>
<?= $content ?>

<?php $this->endContent();