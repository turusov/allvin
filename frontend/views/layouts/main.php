<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\filter\PartsFilter;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1" />
   
   <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    


    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
    <meta name="yandex-verification" content="73130f4b978f391b" />

 <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
</head>
</head>
<body class="smoothscroll enable-animation" >
<?php $this->beginBody() ?>
   <div id="wrapper">
      <div id="header" class="sticky clearfix">
         <header id="topNav">
            <div class="container">
               <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                  <i class="fa fa-bars"></i>
               </button>
               <?=\frontend\widgets\userBlock\userBlock::widget(); ?>
               <a class="logo pull-left" href="/">
                        <?
                        echo Yii::$app->easyImage->thumbOf('/template/images/logo.png',
                            [
                                'quality' => 40,
                                 // 'background' => '#ffffff',
    'type' => 'png',
                            ],
                            [
                              
                                'alt'=> 'logo allvinexpress',
                                'title' => 'logo'
                            ]

                        );
                        ?>
                
                
                <img alt="AllVinExpress логотип" src="/template/images/logo.png">
               </a>
               <div class="navbar-collapse pull-left nav-main-collapse collapse">
                  <nav class="nav-main">
                     <ul id="topMain" class="nav nav-pills nav-main text-uppercase">
                        <li>
                           <a href="/">Главная</a>
                        </li>
                        <li>
                           <a href="/parts">Все запчасти</a>
                        </li>
                        <li>
                           <a href="/brand">Бренды</a>
                        </li>

                        <li>
                           <a href="/marks">Марки</a>
                        </li>
                     </ul>

                  </nav>
               </div>

            </div>
         </header>
         <!-- /Top Nav -->

      </div>



      <?= $content ?>

      <footer id="footer">
         <div class="copyright">
            <div class="container">
               AllVinExpress © 2016
               <div class="pull-right">
               <!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img alt='counter' src='//counter.yadro.ru/hit?t44.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->

               </div>
            </div>
         </div>
         
      </footer>
   </div>

   <a href="#" id="toTop"></a>


<script type="text/javascript">var plugin_path = '/template/plugins/';</script>

<noindex>
<script src="https://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script> 
<script type="text/javascript"> 
    try { var yaCounter41898459 = new Ya.Metrika({ id:41898459, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } 
    </script> 
    <noscript>
    <div>
        <img src="https://mc.yandex.ru/watch/41898459" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript> <!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89942779-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Top100 (Kraken) Counter -->
<script>
    (function (w, d, c) {
    (w[c] = w[c] || []).push(function() {
        var options = {
            project: 4460703
        };
        try {
            w.top100Counter = new top100(options);
        } catch(e) { }
    });
    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src =
    (d.location.protocol == "https:" ? "https:" : "http:") +
    "//st.top100.ru/top100/top100.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(window, document, "_top100q");
</script>

<!-- END Top100 (Kraken) Counter -->
</noindex>
<?php
/**
 * Связанные списки
 */
 
 
$script = <<< JS
   jQuery(document).ready(function() {
      jQuery('.custom-multiselect').multiselect();

   });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<?
    if (!$_SESSION['beta']) {
     $script = <<< JS
   jQuery(document).ready(function() {
         _toastr("Сайт работает в тестовом режиме, некоторый функционал может работать неправильно. Приносим свои извинения за предоставленные неудобства!","top-right","info",false);
   });
JS;
        $this->registerJs($script, yii\web\View::POS_READY);
        $_SESSION['beta'] = 1;
    }
?>
<?=\frontend\widgets\notificationWidget\NotificationWidget::Widget(); ?>
<?php $this->endBody() ?>


</body>
</html>
<?php $this->endPage() ?>


