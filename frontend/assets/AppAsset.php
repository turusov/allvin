<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       // 'css/site.css',
        'template/plugins/slider.revolution/css/extralayers.css',
        'template/plugins/slider.revolution/css/settings.css',
        'template/css/color_scheme/blue.css',
        'template/css/essentials.css',
        'template/css/layout.css',
        'template/css/header-1.css',
        'template/css/layout-shop.css',
        '/template/plugins/bootstrap-multiselect-master/css/bootstrap-multiselect.css'
    ];
    public $js = [
        'template/js/scripts.js',

        'template/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js',
        'template/js/view/demo.revolution_slider.js',
        'template/plugins/smoothscroll.js ',
        'template/js/view/demo.layerslider_slider.js',
        'template/plugins/slider.layerslider/js/layerslider_pack.js',
        '/template/plugins/bootstrap-multiselect-master/js/bootstrap-multiselect.js',
        '/template/plugins/goodshare/goodshare.min.js'
       
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
