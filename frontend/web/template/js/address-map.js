var map, mapRoute;
navigator.geolocation.getCurrentPosition(function(position) {
    do_something(position);
});
var pos;
function do_something(position){
    pos = position;
}
ymaps.ready(function() {
    map = new ymaps.Map('map', {
        center: [pos.coords.latitude, pos.coords.longitude],
        zoom: 12,
        controls: true,
    });
    map.controls.add(
        new ymaps.control.ZoomControl()
    );
    map.events.add('click', function (e) {
            map.geoObjects.each(function(context) {
                map.geoObjects.remove(context);
            });
            var coords = e.get('coordPosition');
            $('#ShopAddress_map_x').val(coords[0]);
            $('#ShopAddress_map_y').val(coords[1]);
            myPlacemark1 = new ymaps.Placemark(coords),
            map.geoObjects.add(myPlacemark1)

    });

});


