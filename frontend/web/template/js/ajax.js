$(function() {
    $('.category-event .status').click(function(){
        swal({
            title: "Вы уверены что хотите скрыть категорию?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да",
            closeOnConfirm: false
        }, function () {
            swal("Скрыта");
        });
        return 0;
    });
    $('.category-event .delete').click(function(){
        swal({
            title: "Вы уверены что хотите удалить категорию?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: "admin/categories/delete",
                type: 'POST',
                data: {
                    category: category_id,
                },
                success: function(data){
                    swal(data);
                }
            });

        });
        return 0;
    });
});