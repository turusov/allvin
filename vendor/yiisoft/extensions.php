<?php

$vendorDir = dirname(__DIR__);

return array (
  'budyaga/users' => 
  array (
    'name' => 'budyaga/users',
    'version' => '1',
    'alias' => 
    array (
      '@budyaga/users' => $vendorDir . '/budyaga/users',
    ),
  ),
  'cliff363825/yii2-image' => 
  array (
    'name' => 'cliff363825/yii2-image',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@cliff363825/image' => $vendorDir . '/cliff363825/yii2-image',
    ),
  ),
  'perminder-klair/yii2-dropzone' => 
  array (
    'name' => 'perminder-klair/yii2-dropzone',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kato' => $vendorDir . '/perminder-klair/yii2-dropzone',
    ),
  ),
  '2amigos/yii2-multi-select-widget' => 
  array (
    'name' => '2amigos/yii2-multi-select-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/multiselect' => $vendorDir . '/2amigos/yii2-multi-select-widget',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'wbraganca/yii2-multiselect' => 
  array (
    'name' => 'wbraganca/yii2-multiselect',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@wbraganca/multiselect' => $vendorDir . '/wbraganca/yii2-multiselect/src',
    ),
  ),
  'sadovojav/yii2-image-thumbnail' => 
  array (
    'name' => 'sadovojav/yii2-image-thumbnail',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@sadovojav/image' => $vendorDir . '/sadovojav/yii2-image-thumbnail',
    ),
  ),
  'demi/cropper' => 
  array (
    'name' => 'demi/cropper',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@demi/cropper' => $vendorDir . '/demi/cropper',
    ),
  ),
  'budyaga/yii2-cropper' => 
  array (
    'name' => 'budyaga/yii2-cropper',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@budyaga/cropper' => $vendorDir . '/budyaga/yii2-cropper',
    ),
  ),
  'kop/yii2-scroll-pager' => 
  array (
    'name' => 'kop/yii2-scroll-pager',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kop/y2sp' => $vendorDir . '/kop/yii2-scroll-pager',
    ),
  ),
  'nirvana-msu/yii2-infinite-scroll' => 
  array (
    'name' => 'nirvana-msu/yii2-infinite-scroll',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@nirvana/infinitescroll' => $vendorDir . '/nirvana-msu/yii2-infinite-scroll',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'budyaga/yii2-users' => 
  array (
    'name' => 'budyaga/yii2-users',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@budyaga/users' => $vendorDir . '/budyaga/yii2-users',
    ),
  ),
  'darkcs/yii2-infinite-scroll' => 
  array (
    'name' => 'darkcs/yii2-infinite-scroll',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@darkcs/infinitescroll' => $vendorDir . '/darkcs/yii2-infinite-scroll',
    ),
  ),
);
