
window.onload=function(){
    navigator.geolocation.getCurrentPosition(showPosition); // Запрашиваем местоположение, и в случае успеха вызываем функцию showPosition
    function showPosition(position) {

        var map = new ymaps.Map ("search-map", {
            center: [position.coords.latitude , position.coords.longitude],
        });
        var placemark = new ymaps.Placemark([position.coords.latitude, position.coords.longitude], {
            iconContent: "Я"
        }, {
            balloonCloseButton: false,
            hideIconOnBalloonOpen: false
        });
        map.geoObjects.add(placemark);
    }
}