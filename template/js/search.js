ymaps.ready(function () {

    var myMap,
        service = new GeolocationService(),
        myLocation = service.getLocation({
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 1000
        });
        myLocation.then(function (loc) {
            var myCoords = [loc.latitude, loc.longitude],
            myPlacemark = new ymaps.Placemark(myCoords, {}, {
                iconImageHref: '/uploads/map/geolocation.png',
                iconImageSize: [24, 24],
                iconImageOffset: [-12, -12]
            });
            myMap = new ymaps.Map('map', {
                center: myCoords,
                zoom: loc.zoom || 12,
            });
            objects = ymaps.geoQuery().addToMap(myMap),
                circle = new ymaps.Circle([myCoords, 4000], null, { draggable: false });
                circle.events.add('drag', function () {
                var objectsInsideCircle = objects.searchInside(circle);
                objectsInsideCircle.setOptions('preset', 'twirl#redIcon');
                objects.remove(objectsInsideCircle).setOptions('preset', 'twirl#blueIcon');
            });
            var shopArray;
            $.ajax({
                url: "/api/getshop",
                type: 'POST',
                async: false,
                data: {
                    city_name: "Муром",
                },
                success: function(shop) {
                    shopArray = jQuery.parseJSON(shop);
                }
            })
            var arrayCoords = new Array;
            shopArray.forEach(function(item, i, arr) {
                arrayCoords[i] = [item.map_x, item.map_y];

            });
            var coords = arrayCoords, shop = new ymaps.GeoObjectCollection();
            for (var i = 0; i < coords.length; i++) {
                console.log(shopArray);
                shop.add(new ymaps.Placemark(coords[i], {
                        // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
                        balloonContentHeader: "<a href='/shop/id"+shopArray[i].id_shop+"'>"+shopArray[i].name+"</a>",
                        balloonContentBody: "<b>Адрес:</b> "+ shopArray[i].address,
                        balloonContentFooter: "<a href='/map/?map_x=" + shopArray[i].map_x + "&map_y=" + shopArray[i].map_y + "'>Схема проезда</a>",

                    }
                ));
            }
            myMap.geoObjects.add(circle).add(myPlacemark).add(shop);
    });


});