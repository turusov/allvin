$(function() {
    url_page = location.pathname;
    $('#cartContent .item .remove_item').on("click", function(){
        var item = this
        $.ajax({
            type: 'POST',
            url: '/api/removebasket',
            data: {
                id:  $(this).parents('.item').find('.product-id').val(),

            },
            success:function(info){
                _toastr("Товар был удален","top-right","info",false);

                count = $('.basket .cartContent #cartContent .item').length

                if (count > 2) {
                    $(item).parents('.item').remove();
                    updateItog();
                } else {
                     var str = url_page + ' #wrapper .basket .container .row';
                     $("#wrapper .basket .container ").load(str);
                }
            },
            error: function(data) { // if error occured
                _toastr("Ошибка","top-right","error",false);
            },
            dataType:'html'
        });
        return false;
    });
    $('#cartContent .item .qty input').change(function(){
        var count = $(this).val();
        var item = this;
        $.ajax({
            type: 'POST',
            url: '/api/countbasket',
            data: {
                id:  $(this).parents('.item').find('.product-id').val(),
                count: count,

            },
            success:function(info){
                updateItog()
                var origin_price = $(item).parents('.item').find('.product-price').val();
                $(item).parents('.item').find('.total_price span').text(count * origin_price);
            },
            error: function(data) { // if error occured
                _toastr("Ошибка","top-right","error",false);
            },
            dataType:'html'
        });
    })
    function updateItog() {
        var str = url_page + ' #wrapper .basket .block-itog';
        $("#wrapper .basket .block-itog ").load(str);
        var str = url_page + ' #wrapper #header .quick-cart .quick-cart-box .cart-item';
        $("#wrapper #header .quick-cart .quick-cart-box").load(str);
    }


// var str = url_page + ' #wrapper .basket .block-itog';
//    $("#wrapper .basket .block-itog ").load(str);

});