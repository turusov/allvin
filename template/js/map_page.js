var map, mapRoute;
navigator.geolocation.getCurrentPosition(function(position) {
    do_something(position);
});
var pos;
function do_something(position){
    pos = position;
}
ymaps.ready(function() {
    map = new ymaps.Map('map', {
        center: [pos.coords.latitude, pos.coords.longitude],
        zoom: 12,
        controls: [],
    });
    createRoute();
});

function createRoute() {
    var getArr = parseGetParams();

    var routeFrom = [pos.coords.latitude, pos.coords.longitude];
    var routeTo =  [getArr.map_x, getArr.map_y];
    var myRouter = ymaps.route([routeFrom, routeTo], {

        mapStateAutoApply: true
    });
    myRouter.then(function(route) {
            var points = route.getWayPoints();
            points.get(0).options.set('iconImageHref', '/uploads/map/geolocation.png')
                .set('iconImageSize', [32, 37])
                .set('iconImageOffset', [-16, -37]);
            points.get(0).properties.set('iconContent', '').set({
                balloonContentBody: 'Мое местоположение'
            });;

            points.get(1).options.set('iconImageHref', '/uploads/map/geolocation.png')
                .set('iconImageSize', [32, 37])
                .set('iconImageOffset', [-16, -37]);
            points.get(1).properties.set('iconContent', '').set({
                balloonContentBody: 'Место назначения'
            });;
            route.getPaths().options.set({
                strokeColor: '179BD7',
                opacity: 0.9
            });

            map.geoObjects.add(route);
        },
        // Обработка ошибки
        function (error) {
            _toastr("Ошибка","top-right","error",false);
        }
    )


};
function parseGetParams() {
    var $_GET = {};
    var __GET = window.location.search.substring(1).split("&");
    for(var i=0; i<__GET.length; i++) {
        var getVar = __GET[i].split("=");
        $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
    }
    return $_GET;
}
