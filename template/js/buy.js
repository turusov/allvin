$(function() {
    url_page = location.pathname;
    $('.product-add-cart').click(function(){
        var form = $(this).parents('form').serializeArray();
        console.log(form[0].value);
        $.ajax({
            type: 'POST',
            url: '/api/AddBasket',
            data: {
                id: form[0].value,
                count: form[1].value
            },
            success:function(info){
                _toastr("Товар добавлен в корзину","top-right","primary",false);

                var str = url_page + ' #wrapper #header .quick-cart >a span';
                $("#wrapper #header .quick-cart >a").load(str);

                var str = url_page + ' #wrapper #header .quick-cart .quick-cart-box .cart-item';
                $("#wrapper #header .quick-cart .quick-cart-box").load(str);
            },
            error: function(data) { // if error occured
                _toastr("Ошибка","top-right","error",false);
            },
            dataType:'html'
        });
        return false;
    })
})